<?php

namespace App\Services\Flow;

use App\Dto\AutoTrigger\AutoPullProductDto;
use App\Enums\Flow\FlowEventEnum;
use App\Enums\Flow\FlowEventHandleEnum;
use App\Enums\Flow\FlowFilterEnum;
use App\Enums\Flow\FlowOperationEnum;
use App\Enums\Flow\FlowShopStatusEnum;
use App\Enums\Flow\FlowTypeEnum;
use App\Enums\Merchant\MerchantFinancialTypeEnum;
use App\Enums\Merchant\MerchantFunctionEnum;
use App\Enums\Merchant\MerchantPlanLevelEnum;
use App\Enums\Merchant\MerchantPlanPermissionEnum;
use App\Enums\Merchant\MerchantPlanPermissionNameEnum;
use App\Enums\ResponseCodeEnum;
use App\Enums\Segment\MarketingTypeEnum;
use App\Enums\System\PlatformEnum;
use App\Exceptions\MerchantCenter\UpgradePackageException;
use App\Jobs\AutoPullProductJob;
use App\Lib\Code;
use App\Models\AutoTrigger\EventProductCollection;
use App\Models\CommonModel;
use App\Models\Flow\FlowCustomer;
use App\Models\Flow\FlowShop;
use App\Models\Flow\FlowShopTriggerRule;
use App\Models\Flow\FlowTask;
use App\Services\Account\AccountService;
use App\Services\Flow\Filter\FlowNodeValidFactory;
use App\Services\Flow\Filter\FlowOtherFilterFactory;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class FlowShopService
{
    public $emailTemplateSign = null; // flow流程中的邮件模板uuid
    public $smsFlag = 0;              // 待审核短信模板个数
    public $merchantId = 0;
    public $shopSign = '';

    public function getList($data, $model, $merchantUser)
    {
        $perPage = $data['limit'] ?? CommonModel::PAGE_SIZE;

        $model = $model->whereIn('shop_sign', $merchantUser->shop_signs);
        if(isset($data['name'])) {
            $model = $model->where('name', 'LIKE',  $data['name'] . '%');
        }

        $selectArr = [
            'sign', 'name', 'type', 'status', 'last_updated_at', 'created_at', 'send_num', 'send_user_num', 'click_num',
            'sms_send_num', 'sms_click_num', 'income', 'sms_income', 'sync_unsub_tag', 'shop_sign'
        ];

        return $model->select($selectArr)->orderBy('created_at', 'desc')->paginate($perPage);
    }

    public function getFlowShop($sign, $merchantUser, $isDetail = 1)
    {
        $selectArr = ['id', 'sign', 'name', 'type', 'status', 'last_updated_at', 'sms_flag', 'email_flag', 'shop_sign'];

        if ($isDetail) { // 获取详情 加入rule字段
            array_push($selectArr, 'rule');
        }

        $model = FlowShop::where('sign', $sign)->whereIn('shop_sign', $merchantUser->shop_signs);
        if ($isDetail != 2) {
            $model = $model->select($selectArr);
        }
        if ($isDetail == 2) { // 审批中限制操作
            $model->where('status', '!=', FlowShopStatusEnum::STATUS_TO_APPROVE);
        }

        $model = $model->first();
        if (! $model) {
            throw new \Exception(ResponseCodeEnum::getDescription(Code::UNKOWN_ERROR), Code::UNKOWN_ERROR);
        }
        // 注意新增非model原属性需要在编辑功能中移除
        $model->platform = $merchantUser->shop_platforms->{$model->shop_sign} ?? '';
        $model->platform_text = PlatformEnum::getDescription($model->platform);
        $model->shop_name = getShopInfo($model->shop_sign)->shop_name ?? '';

        return $model;
    }

    public function valid($data, $merchantUser, $action = 'create')
    {
        $this->merchantId = $merchantUser->merchant_id;
        $this->shopSign = $data['shop_sign'] ?? '';

        $data['operation_id'] = $merchantUser->id;
        $data['operation_name'] = $merchantUser->name;
        $data['common_trigger_rule'] = '';
        $data['merchant_id'] = $merchantUser->merchant_id;
        if (!empty($data['rule'])) {
            $data['last_updated_at'] = date('Y-m-d H:i:s');
            $data['log'] = [
                'origin_rule' => $data['rule'],
                'rule' => $this->formatRule($data['format_rule'], $data['type']),
                'shop_sign' => $data['shop_sign'],
                'create_user' => $merchantUser->name,
                'current_status' => $data['status'],
            ];
            // 抽出触发器公共规则
            $data = $this->setCommonTriggerRule($data, $data['shop_sign'], $data['type']);
            $data['sms_flag'] = $this->smsFlag;                                   // 待审核规则中含有短信节点个数
            $data['email_flag'] = count(array_unique($this->emailTemplateSign));  // 待审核规则中含有邮件模板个数
        } else if (isset($data['rule'])) {
            unset($data['rule']);
        }

        if ($data['sms_flag'] || $data['email_flag']) {
            $accountService = new AccountService();
            $data['sms_flag'] && $accountService->balanceCheck($merchantUser->merchant_id, MerchantFinancialTypeEnum::Sms, 1, '', MarketingTypeEnum::Flow);
            $data['email_flag'] && $accountService->balanceCheck($merchantUser->merchant_id, MerchantFinancialTypeEnum::Email, 1, '', MarketingTypeEnum::Flow);
        }

        // 用到的系列
        $eventProductCollections = [];
        if (!empty($data['collection_ids']) && is_array($data['collection_ids'])) {
            $existsCollectionIds = EventProductCollection::whereIn('collection_id', $data['collection_ids'])->pluck('collection_id')->toArray();
            $insertCollectionIds = array_diff($data['collection_ids'], $existsCollectionIds);
            foreach ($insertCollectionIds as $insertCollectionId) {
                $dtoData = ['shop_sign' => $data['shop_sign'], 'collection_id' => $insertCollectionId];
                dispatch(new AutoPullProductJob(AutoPullProductDto::fromItem($dtoData)));
                $dtoData['created_at'] = date('Y-m-d H:i:s');
                array_push($eventProductCollections, $dtoData);
            }
        }

        $data['event_product_collections'] = $eventProductCollections;

        // 发布状态且含有短信或者邮件节点接入审批
        if (isset($data['status']) && $data['status'] == FlowShopStatusEnum::STATUS_ENABLE
            && ($data['sms_flag'] > 0 || $data['email_flag'] > 0)) {
            $data['status'] = FlowShopStatusEnum::STATUS_TO_APPROVE;
        }

        return $data;
    }

    public function formatRule($rule, $type)
    {
        $rule = $this->validFormatRule($rule, $type);

        // 添加额外必须处理的条件
        $rule = $this->otherMergeFilter($rule);

        return $rule;
    }

    public function validFormatRule($rule, $type)
    {
        // 校验开始节点
        $eventCounts = array_count_values(array_column($rule, 'event'));
        $startCount = $eventCounts[FlowEventEnum::StartConditional] ?? 0;

        if ($startCount == 0 || $startCount > 1) {
            throw new \Exception(__('filter.flow_start_count_max'), Code::COMMON_TIP);
        }

        $messageCount = $eventCounts[FlowEventEnum::MessageEvent] ?? 0;
        if ($messageCount == 0) {
            throw new \Exception(__('filter.flow_message_event_exists'), Code::COMMON_TIP);
        }

        // 条件节点5个
//        $startCount = $eventCounts[FlowEventEnum::ConditionalEvent] ?? 0;
//        if ($startCount > 5) {
//            throw new \Exception(__('filter.flow_conditional_count_max'), Code::COMMON_TIP);
//        }

        // 验证行动
        $triggerHandles = array_column(config('flow.triggers'), 'handles', 'type')[$type] ?? [];
        // 添加的行动
        $actionHandles = array_unique(array_filter(array_column($rule, 'handle')));
        $diffHandles = array_diff($actionHandles, $triggerHandles);
        if (!empty($diffHandles)) {
            throw new \Exception('invalid action : ' . implode(',', $diffHandles), Code::COMMON_TIP);
        }

        // 校验节点不能回流
        $nodes = array_column($rule, null, 'id');
        $this->validNode($nodes);

        // 消息节点内容是否合规
        $nodes = $this->validMessageNode($nodes);

        return array_values($nodes);
    }

    /**
     * 校验节点不能回流
     *
     * User: pancake
     * DateTime: 2022/12/13 17:54
     * @param $nodes
     * @param int $id
     * @param int $num
     * @return bool
     * @throws \Exception
     */
    public function validNode($nodes, $id = 0, $num = 0)
    {
        $node = $id == 0 ? current($nodes) : ($nodes[$id] ?? []);
        if (empty($node)) {
            return true;
        }

        $num++;
        if ($num > count($nodes)) {
            throw new \Exception(__('filter.flow_nodes_valid'), Code::COMMON_TIP);
        }

        $outs = array_merge(($node['out'] ?? []), ($node['nout'] ?? []));
        if (empty($outs)) {
            return true;
        }

        foreach ($outs as $out) {
            if (empty($out)) {
                continue ;
            }
            $this->validNode($nodes, $out, $num);
        }
    }

    /**
     * 验证消息节点内容是否合规
     *
     * User: pancake
     * DateTime: 2022/12/13 17:54
     * @param $nodes
     * @return mixed
     * @throws \Exception
     */
    public function validMessageNode($nodes)
    {
        $this->emailTemplateSign = [];
        $this->smsFlag = 0;
        $iniData = ['merchant_id' => $this->merchantId, 'shop_sign' => $this->shopSign];
        foreach ($nodes as &$node) {
            if ($node['event'] == FlowEventEnum::MessageEvent) {
                try {
                    FlowNodeValidFactory::getObject($node['handle'])->iniParam($iniData)->validNode($node);
                    if (!empty($node['data']['email_template_sign'])) { // 抽出店铺邮件模板uuid
                        array_push($this->emailTemplateSign, $node['data']['email_template_sign']);
                    }
                    if (FlowEventHandleEnum::SendSms == $node['handle']) {
                        $this->smsFlag++;
                    }
                } catch (\Exception $e) {
                    if ($e->getMessage() != 'invalid type') {
                        throw $e;
                    }
                }
            }
        }

        return $nodes;
    }

    public function flowConfig()
    {
        $config = config('flow');

        // 事件
        foreach ($config['events'] as &$event) {
            $event['name'] = FlowEventEnum::getDescription($event['event']);
        }

        // 触发器
        foreach ($config['triggers'] as &$trigger) {
            $trigger['name'] = FlowTypeEnum::getDescription($trigger['type']);
            $trigger['desc'] = FlowTypeEnum::getLocalizedDescriptionAll($trigger['type']);
        }

        // 行动，消息事件
        foreach ($config['Message_Intermediate_Event'] as &$message) {
            $message['name'] = FlowEventHandleEnum::getDescription($message['handle']);
            $message['desc'] = FlowEventHandleEnum::getLocalizedDescriptionAll($message['handle']);
            $message['tip'] = FlowEventHandleEnum::getLocalizedDescriptionTip($message['handle']);
        }

        // 操作器
        foreach ($config['operations'] as &$operations) {
            foreach ($operations as &$operation) {
                $operation['label'] = FlowOperationEnum::getDescription($operation['name']);
            }
        }

        // 条件
        foreach ($config['filters'] as $key => &$filter) {
            if (isset($filter['is_show']) && $filter['is_show'] == 0) { // 不展示直接屏蔽
                unset($config['filters'][$key]);
                continue;
            }
            if (App::getLocale() == 'ch') {
                $filter['name'] = $filter['cn_name'];
            }
            $filter['label'] = FlowFilterEnum::getLocalizedDescriptionAll($key);
            unset($filter['service']);
        }

        return $config;
    }

    public function getRule($originRules)
    {
        $rules = [];
        foreach ($originRules as $originRule) {
            $this->getNodes($originRule, $rules);
        }

        return $rules;
    }

    public function getNodes($node, &$nodes = [])
    {
        $ruleNode = $this->filterNodeField($node);
        array_push($nodes, $ruleNode);

        if (empty($node['childNode'])) {
            return false;
        }

        $node['event'] = $node['event'] ?? '';
        // 条件节点
        $childNodes = $node['childNode'];
        if ($node['event'] == FlowEventEnum::ConditionalEvent) {
            $childNodes = $node['childNode']['conditionNodes'] ?? [];
        }

        // 判断节点是否多个
        if (key($childNodes) != 0) {
            return $this->getNodes($childNodes, $nodes);
        }

        foreach ($childNodes as $childNode) {
            if ($node['event'] == FlowEventEnum::ConditionalEvent) {
                if (!empty($childNode['childNode'])) $this->getNodes($childNode['childNode'], $nodes);
            } else {
                $this->getNodes($childNode, $nodes);
            }
        }
    }

    public function filterNodeField($node)
    {
        $fields = ['id', 'event', 'out', 'in', 'nout', 'sleep', 'handle', 'data', 'filter', 'rule', 'shop_sign'];
        foreach ($node as $key => $value) {
            if (!in_array($key, $fields)) unset($node[$key]);
        }
        return $node;
    }

    /**
     * @desc 统计发送总数
     * 包括发送数量统计 + 发送人统计去重（用户+店铺+流程类型）
     * 记得调用方发起事务
     * @param $shopSign
     * @param $flowSign
     * @param $customerId
     */
    public function sumSendNum($shopSign, $flowSign, $customerId)
    {
        $updateColumn = [
            'send_num' => DB::raw("send_num + 1")
        ];
        $columns = ['shop_sign' => $shopSign,
            'flow_shop_sign' => $flowSign,
            'customer_id' => $customerId
        ];
        $customerInfo = FlowCustomer::where($columns)->first();
        if (!$customerInfo) {
            FlowCustomer::create($columns);
            $updateColumn['send_user_num'] = DB::raw("send_user_num + 1");
        }
        FlowShop::where(['sign' => $flowSign, 'shop_sign' => $shopSign])->update($updateColumn);
        return true;
    }

    /**
     * @desc 获取店铺有效的流程模板
     * @param $shopSign
     * @param $action
     * @return array
     */
    public function getValidShopFlow($shopSign, $action)
    {
        $selectColumn = ['id', 'name', 'type', 'sign', 'marketing_no','flow_template_sign', 'shop_sign', 'last_updated_at',
            'status', 'created_at', 'updated_at', 'send_user_num', 'merchant_id', 'operation_id', 'operation_name'];
        return FlowShop::select($selectColumn)->where(['shop_sign' => $shopSign, 'type' => $action, 'status' => FlowShopStatusEnum::STATUS_ENABLE])
            ->orderBy('created_at')->get()->toArray();
    }

    /**
     * @desc 设置当前编辑状态下一个规则下的触发器规则（供后端逻辑处理使用）
     * 同一个触发器下的所有flow共享，以最新的触发器规则为准
     * @param array   $data 全部规则
     * @param string  $shopSign
     * @param integer $type
     * @return array
     */
    private function setCommonTriggerRule($data, $shopSign, $type)
    {
        $commonRule = [];
        foreach ($data['log']['rule'] as &$row) {
            if ($row['event'] != 'Conditional_Start_Event' || empty($row['rule'])) {
                continue;
            }
            // 初始化全局规则
            $commonRule = ['shop_sign' => $shopSign, 'trigger_type' => $type, 'trigger_rule' => []];
            // 抽出触发器规则的次数与间隔 作为全局规则使用
            foreach ($row['rule'] as &$items) {
                foreach ($items as $itemKey => $item) {
                    if (in_array($item['filter_field'], ['trigger_event_num', 'trigger_not_event_day'])) {
                        array_push($commonRule['trigger_rule'], $item);
                        unset($items[$itemKey]);
                    }
                }
            }

            break;// 触发器规则只有一个
        }

        $data['common_trigger_rule'] = $commonRule;

        return $data;
    }

    /**
     * @desc 获取当前触发器公共规则 提供给前台页面展示使用
     *  同一个触发器下的flow共用
     * @param array   $rule flow规则
     * @param string  $shopSign
     * @param integer $type
     * @return array
     */
    public function getCommonTriggerRule($rule, $shopSign, $type)
    {
        $where = ['shop_sign' => $shopSign, 'trigger_type'=> $type];
        $commonTriggerRule = FlowShopTriggerRule::where($where)->first(['trigger_rule']);
        if (!empty($commonTriggerRule->trigger_rule)) { // 若设置了全局触发器规则 则 在显示详情时直接合并展示即可
            foreach ($rule as &$row) {
                if ($row['event'] == 'Conditional_Start_Event' && !empty($row['rule'])) {
                    // 剔除旧数据
                    foreach ($row['rule'] as &$items) {
                        foreach ($items as $ki => $item) {
                            if (in_array($item['filter_field'], ['trigger_event_num', 'trigger_not_event_day'])) {
                                unset($items[$ki]);
                            }
                        }
                    }
                    // 替换全局统一规则
                    $row['rule'][0] = array_merge($row['rule'][0], $commonTriggerRule->trigger_rule);
                    break; // 触发器规则只有一个
                }
            }
        }

        return $rule;
    }

    /**
     * @desc 保存触发器规则
     * @param array $data
     * @return boolean
     */
    public function saveTriggerRule($data)
    {
        if (!empty($data)) {
            $where = ['shop_sign' => $data['shop_sign'],
                'trigger_type' => $data['trigger_type']
            ];
            FlowShopTriggerRule::updateOrCreate($where, $data);
        }

        return true;
    }

    /**
     * @desc 针对其他一些无需用户设置但必须校验的逻辑 可直接合并进入过滤条件中统一处理
     * @param $nodes
     * @return mixed
     */
    public function otherMergeFilter($nodes)
    {
        foreach (FlowOtherFilterFactory::$map as $severClassName) {
            app()->make($severClassName)->mergeFilter($nodes);
        }

        return $nodes;
    }


}
