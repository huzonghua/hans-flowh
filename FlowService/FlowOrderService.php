<?php


namespace App\Services\Flow;


use App\Enums\Flow\FlowOrderStatusEnum;
use App\Enums\Flow\FlowTypeEnum;
use App\Http\Resources\Flow\FlowOrderListResource;
use App\Lib\Code;
use App\Models\Flow\FlowOrder;
use App\Models\Shopifys\ShopifyOrder;

class FlowOrderService
{
    /**
     * 创建流 业务单据
     * @param $requestData
     * @return mixed
     */
    public function addFlowOrder($request)
    {
        $flowShopId = $request['flow']['id'];
        $flowShopLogSign = md5($request['flow']['id'].''.$request['flow']['last_updated_at']);
        $flowShopType = $request['flow']['type'];

        $flowOrderParam = [
            'sign' => getSign(),
            'shop_sign' => $request['shop_sign'],
            'customer_id' => $request['customer_id'],
            'flow_shop_id' => $flowShopId,
            'flow_shop_log_sign' => $flowShopLogSign,
            'type' => $flowShopType,
            'business_id' => isset($request['legacy_resource_id']) ? (string)$request['legacy_resource_id'] : $request['customer_id'],
            'business_data' => $request,
            'flow_node' => '',
            'flow_status' => $request['flow_status'] ?? FlowOrderStatusEnum::WAITING,
        ];
        $flowOrder = FlowOrder::create($flowOrderParam);
        if (empty($flowOrder)) {
            throw new \Exception('create flow order error.', Code::FLOW_ORDER_CREATE_ERROR);
        }

        return $flowOrder;
    }

    /**
     * @desc 更新flow 业务单据状态
     * @param $flowOrderId
     * @param $flowStatusCode
     * @param string $currentNodeId
     */
    public function updateFlowOrderStatus($flowOrderId, $flowStatusCode, $currentNodeId = '')
    {
        $result = match($flowStatusCode) {
            100 => FlowOrderStatusEnum::DOING,
            200 => FlowOrderStatusEnum::SUCCESS,
            400 => FlowOrderStatusEnum::RULE_FAIL,
            500 => FlowOrderStatusEnum::FAIL,
        };
        $updateColumn = ['flow_status' => $result,
            'flow_node' => (string)($flowStatusCode != 200 ? $currentNodeId : ''),
        ];

        return FlowOrder::where(['id' => $flowOrderId])->update($updateColumn);
    }

    /**
     * @desc 订单支付 获取对应弃单流程单据
     * @param $request
     * @return mixed
     */
    public function getCheckoutFlowOrders($request)
    {
        $checkout = ShopifyOrder::where(['legacy_resource_id' => $request['legacy_resource_id'], 'shop_sign' => $request['shop_sign']])
            ->first(['checkout_id']);
        if (empty($checkout)) {
            return [];
        }
        $checkout = $checkout->toArray();
        $whereArr = [
            'shop_sign' => $request['shop_sign'],
            'business_id' => $checkout['checkout_id'],
            'type' => FlowTypeEnum::Checkout,
            'customer_id' =>  $request['customer_id']
        ];

        return FlowOrder::select('id')
            ->where($whereArr)
            ->whereIn('flow_status', [FlowOrderStatusEnum::DOING, FlowOrderStatusEnum::WAITING, FlowOrderStatusEnum::SUCCESS])
            ->get()->toArray();
    }

    /**
     * @desc 流程单据终止.
     * @param $idArr
     * @return mixed
     */
    public function cancelFlow($idArr)
    {
        return FlowOrder::whereIn('id', $idArr)
            ->whereIn('flow_status', [FlowOrderStatusEnum::DOING, FlowOrderStatusEnum::WAITING])
            ->update(['flow_status' => FlowOrderStatusEnum::FLOW_TERMINAL]);
    }

    /**
     * @desc 获取flow触发的单据
     * @param $data
     * @param $model
     * @param $merchantUser
     * @return FlowOrderListResource
     */
    public function getList($data, $model, $merchantUser)
    {
        $perPage = $data['limit'] ?? CommonModel::PAGE_SIZE;
        $model = $model->with('subscriberInfo')
            ->whereIn('shop_sign', $merchantUser->shop_signs)
            ->where('flow_shop_id', $data['flow_shop_id']);

        $selectArr = [
            'id', 'sign', 'shop_sign', 'customer_id', 'flow_shop_id', 'business_id', 'flow_status',
            'run_remark', 'created_at'
        ];

        $data = $model->select($selectArr)->orderBy('created_at', 'desc')->paginate($perPage);

        return new FlowOrderListResource($data);
    }

}
