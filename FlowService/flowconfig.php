<?php

return
[
    // 节点事件 see bpmn 标准事件 https://www.jianshu.com/p/dd7df9613bc1
    "events" => [
        // [
        //     "event" => "Start Event",
        //     "name" => "开始事件",
        //     "desc" => "",
        // ],
        [
            "event" => "Conditional_Start_Event",
            "name" => "条件开始事件",
            "desc" => "",
        ],
        [
            "event" => "Conditional_Intermediate_Event",
            "name" => "条件事件",
            "desc" => "",
        ],
        [
            "event" => "Timer_Intermediate_Event",
            "name" => "延迟事件",
            "desc" => "",
        ],
        [
            "event" => "Message_Intermediate_Event",
            "name" => "消息事件",
            "desc" => "",
        ],
        [
            "event" => "None_End_Event",
            "name" => "结束事件",
            "desc" => "",
        ],
    ],

    // 触发器
    "triggers" => [
        [
            "trigger" => "new/user",
            "type" => 6,
            "name" => "new/user",
            "cn_name" => "新用户加入",
            "filters" => ["customer_sms_source"],
            "handles" => ["sendSms", "customerTags", "sendEmail"],
        ],
        [
            "trigger" => "checkouts/create",
            "type" => 1,
            "name" => "checkouts/create",
            "cn_name" => "放弃支付",
            "filters" => ["brought_product", "collection", "checkout_created_at"],
            "handles" => ["sendSms", "customerTags", "sendEmail"],
        ],
        [
            "trigger" => "orders/paid",
            "type" => 2,
            "name" => "orders/paid",
            "cn_name" => "支付成功",
            "filters" => ["brought_product", "collection", "order_created_at"],
            "handles" => ["sendSms", "customerTags", "orderTags", "sendEmail"],
        ],
        [
            "trigger" => "orders/cancelled",
            "type" => 3,
            "name" => "orders/cancelled",
            "cn_name" => "取消订单",
            "filters" => ["brought_product", "collection", "order_created_at", "order_cancel_reason"],
            "handles" => ["sendSms", "customerTags", "orderTags", "sendEmail"],
        ],
        [
            "trigger" => "orders/delivery",
            "type" => 4,
            "name" => "orders/delivery",
            "cn_name" => "订单发货",
            "filters" => ["brought_product", "collection", "order_created_at"],
            "handles" => ["sendSms", "customerTags", "orderTags", "sendEmail"],
        ],
        [
            "trigger" => "orders/refunded",
            "type" => 5,
            "name" => "orders/refunded",
            "cn_name" => "订单退款",
            "filters" => ["brought_product", "collection"],
            "handles" => ["sendSms", "customerTags", "orderTags", "sendEmail"],
        ],
    ],

    "Conditional_Intermediate_Event" => [
        [
            'conditional' => 'customer',
            'name' => '用户过滤',
            'desc' => '只允许特定的用户触发流程',
            'filters' => ['orders_count', 'last_order_at', 'total_spent', 'segment', 'customer_status', 'customer_tag', 'specific_country', 'customer_sms_source', 'brought_product'],
        ]
    ],

    // 过滤条件
    "filters" => [
        // 购买产品
        'brought_product' => [
            'value' => 'brought_product',
            'name' => 'Products purchased',
            'cn_name' => '购买产品',
            'validate_field' => ['brought_product', 'contain'],
            'operation' => ['contain_picker'],
            "type" => "array",
            'max' => 10,
            'service' => 'App\Services\Flow\Filter\Order\FlowFilterBroughtProductService'
        ],
        // 产品系列
        'collection' => [
            'value' => 'collection',
            'name' => 'Product collection',
            'cn_name' => '产品系列',
            'validate_field' => ['collection', 'contain'],
            'operation' => ['contain_picker'],
            "type" => "array",
            'max' => 5,
            'service' => 'App\Services\Flow\Filter\Order\FlowFilterCollectionService'
        ],
        // 弃单创建时间
        'checkout_created_at' => [
            'value' => 'checkout_created_at',
            'name' => 'Checkout created_at',
            'cn_name' => '弃单创建时间',
            'validate_field' => ['filter_period'],
            'operation' => ['date_picker'],
            "type" => "date",
            'service' => 'App\Services\Flow\Filter\Order\FlowFilterCheckoutCreatedAtService'
        ],
        // 订单创建时间
        'order_created_at' => [
            'value' => 'order_created_at',
            'name' => 'Order created_at',
            'cn_name' => '订单创建时间',
            'validate_field' => ['filter_period'],
            'operation' => ['date_picker'],
            "type" => "date",
            'service' => 'App\Services\Flow\Filter\Order\FlowFilterOrderCreatedAtService'
        ],
        // 订单取消原因
        'order_cancel_reason' => [
            'value' => 'order_cancel_reason',
            'name' => 'Order cancel reason',
            'cn_name' => '订单取消原因',
            'validate_field' => ['cancel_reason', 'contain'],
            'operation' => ['contain_picker'],
            "type" => "string",
            'max' => 1,
            'service' => 'App\Services\Flow\Filter\Order\FlowFilterOrderCancelReasonService'
        ],
        //-------------------用户条件---------------------//
        // 顾客订单数量
        'orders_count' => [
            'value' => 'orders_count',
            'name' => 'Order quantity',
            'cn_name' => '订单数量',
            'validate_field' => ['filter_operation', 'filter_period'],
            'operation' => ['number_check', 'customer_date_picker'],
            "type" => "number",
            'service' => 'App\Services\Flow\Filter\Customer\FlowFilterOrderCountService'
        ],
        // 最近一笔消费时间(多少天内)
        'last_order_at' => [
            'value' => 'last_order_at',
            'name' => 'Last order at',
            'cn_name' => '最后一笔订单创建时间',
            'validate_field' => ['filter_operation'],
            'operation' => ['number_check'],
            "type" => "date",
            'service' => 'App\Services\Flow\Filter\Customer\FlowFilterLastOrderAtService'
        ],
        //总消费金额
        'total_spent' => [
            'value' => 'total_spent',
            'name' => 'Total spent',
            'cn_name' => '消费金额',
            'validate_field' => ['filter_operation'],
            'operation' => ['number_check', 'customer_date_picker'],
            "type" => "number",
            'service' => 'App\Services\Flow\Filter\Customer\FlowFilterTotalSpentService',
        ],
        // 顾客用户分类
        'segment' => [
            'value' => 'segment',
            'name' => 'User segment',
            'cn_name' => '用户分类',
            'validate_field' => ['customer_segment', 'contain'],
            'operation' => ['contain_picker'],
            "type" => "array",
            'max' => 5,
            'service' => 'App\Services\Flow\Filter\Customer\FlowFilterSegmentService',
        ],
        // 顾客状态
        'customer_status' => [
            'value' => 'customer_status',
            'name' => 'Customer status',
            'cn_name' => '顾客状态',
            'validate_field' => ['customer_status', 'contain'],
            'operation' => ['contain_picker'],
            "type" => "string",
            'max' => 1,
            'service' => 'App\Services\Flow\Filter\Customer\FlowFilterCustomerStatusService',
        ],
        // 顾客标签
        'customer_tag' => [
            'value' => 'customer_tag',
            'name' => 'Customer tag',
            'cn_name' => '顾客标签',
            'validate_field' => ['customer_tag', 'contain'],
            'operation' => ['contain_picker'],
            "type" => "array",
            'max' => 5,
            'service' => 'App\Services\Flow\Filter\Customer\FlowFilterCustomerTagService',
        ],

        //特定国家(州)
        'specific_country' => [
            'value' => 'specific_country',
            'name' => 'Customer country',
            'cn_name' => '顾客所属国家',
            'validate_field' => ['province', 'country'],
            'operation' => ['specific_country'],
            "type" => "string",
            'service' => 'App\Services\Flow\Filter\Customer\FlowFilterSpecificCountryService',
        ],
        // 顾客短信订阅来源
        'customer_sms_source' => [
            'value' => 'customer_sms_source',
            'name' => 'Subscribe source',
            'cn_name' => '顾客订阅来源',
            'validate_field' => ['customer_sms_source', 'contain'],
            'operation' => ['contain_picker'],
            "type" => "string",
            'max' => 1,
            'service' => 'App\Services\Flow\Filter\Customer\FlowFilterSmsSourceService'
        ],
        // 顾客短信订阅时间
        'customer_sms_sub_at' => [
            'value' => 'customer_sms_sub_at',
            'name' => 'SMS subscribed at',
            'cn_name' => '短信订阅时间',
            'validate_field' => ['filter_operation'],
            'operation' => ['customer_date_picker'],
            "type" => "date",
            'service' => 'App\Services\Flow\Filter\Customer\FlowFilterSmsSubAtService',
        ],
        // 购买指定产品的订单数 此节点不在前端展示仅作后端逻辑处理使用
        'contain_product_order_num' => [
            'value' => 'product_orders_count',
            'name' => 'Contains the order quantity of the product',
            'cn_name' => '包含产品的订单数量',
            'validate_field' => ['filter_operation', 'filter_period', 'brought_product', 'contain'],
            'operation' => ['number_check', 'customer_date_picker', 'contain_picker'],
            'type' => 'number',
            'service' => 'App\Services\Flow\Filter\Order\FlowFilterContainProductOrderNumService',
            'is_show' => 0,
        ],
        // 用户是否订阅 包含 短信或者邮件
        'customer_subscriber_type' => [
            'value' => 'customer_subscriber_type',
            'name' => 'subscriber type',
            'cn_name' => '用户是否订阅',
            'validate_field' => ['contain'],
            'operation' => ['contain_picker'],
            'type' => ['contain_picker'],
            'max' => 1,
            'service' => 'App\Services\Flow\Filter\Customer\FlowFilterSubscribeTypeService',
            'is_show' => 0,
        ],
        //-------------------用户条件---------------------//
        //-------------------触发规则---------------------//
        // 同一触发器能被同一用户触发次数
        'trigger_event_num' => [
            'value' => 'trigger_event_num',
            'name' => 'The number of times triggered by the same customer',
            'cn_name' => '同一用户触发的次数',
            'validate_field' => ['filter_value'],
            'operation' => ['number_check'],
            "type" => "number",
            'service' => 'App\Services\Flow\Filter\Rule\FlowFilterRuleService',
        ],
        // 同一流程能被同一用户触发次数
        'flow_event_num' => [
            'value' => 'flow_event_num',
            'name' => 'The number of times triggered by the same customer',
            'cn_name' => '同一用户触发的次数',
            'validate_field' => ['filter_value'],
            'operation' => ['number_check'],
            "type" => "number",
            'service' => 'App\Services\Flow\Filter\Rule\FlowFilterRuleService',
        ],
        // 多少时间内客户不能触发同一触发器
        'trigger_not_event_day' => [
            'value' => 'trigger_not_event_day',
            'name' => 'The time interval triggered by the same customer',
            'cn_name' => '同一用户触发的时间间隔',
            'validate_field' => ['filter_value'],
            'operation' => ['number_check'],
            "type" => "number",
            'service' => 'App\Services\Flow\Filter\Rule\FlowFilterRuleService',
        ],
        // 多少时间内客户不能触发同一流程
        'flow_not_event_day' => [
            'value' => 'flow_not_event_day',
            'name' => 'The time interval triggered by the same customer',
            'cn_name' => '同一用户触发的时间间隔',
            'validate_field' => ['filter_value'],
            'operation' => ['number_check'],
            "type" => "number",
            'service' => 'App\Services\Flow\Filter\Rule\FlowFilterRuleService',
        ],
        //-------------------触发规则---------------------//
    ],


    "rule_filters" => [
        "trigger_event_num", "flow_event_num", "trigger_not_event_day", "flow_not_event_day"
    ],

    // 行动
    "Message_Intermediate_Event" => [
        [
            "name" => "Send SMS",
            "desc" => "发送短信给触发流程的用户",
            "handle" => "sendSms",
            "tip" => "",
        ],
        [
            "name" => "customer tag",
            "desc" => "给触发流程的对应用户添加标签",
            "handle" => "customerTags",
            "tip" => "标签添加成功后再shopify-客户-标签处查看",
        ],
        [
            "name" => "order tag",
            "desc" => "给触发流程的对应订单添加标签",
            "handle" => "orderTags",
            "tip" => "标签添加成功后再shopify-订单-标签处查看",
        ],
        [
            "name" => "Send email",
            "desc" => "发送邮件给触发流程的用户",
            "handle" => "sendEmail",
            "tip" => "",
        ],
    ],

    // 操作
    "operations" => [
        //包含不包含
        'contain_picker' => [
            'contains' => [
                'value' => 'in',
                'name' => 'contains',
                'validate_field' => [['name' => 'value', 'type' => 'array']]
            ],
            'not_contains' => [
                'value'=> 'not in',
                'name' => 'not_contains',
                'validate_field' => [['name' => 'value', 'type' => 'array']]
            ]
        ],

        // customer_date_picker
        'customer_date_picker' => [
            // 指定日期
            'exact_date' => [
                'name' => 'exact_date',
                'value' => '=',
                'validate_field' => [['name' => 'value', 'type' => 'date']]
            ],
            // 指定日期之间
            'between' => [
                'name' => 'between',
                'value' => 'between',
                'validate_field' => [['name' => 'start_date', 'type' => 'date'], ['name' => 'end_date', 'type' => 'date']] //操作需要验证的字段
            ],
            // 指定日期之前
            'on_or_before' => [
                'name' => 'on_or_before',
                'value' => '<=',
                'validate_field' => [['name' => 'value', 'type' => 'date']]
            ],
            // 指定日期之后
            'on_or_after' => [
                'name' => 'on_or_after',
                'value' => '>=',
                'validate_field' => [['name' => 'value', 'type' => 'date']]
            ],
            // 触发日期之前
            'trigger_before' => [
                'name' => 'trigger_before',
                'value' => 'between',
                'validate_field' => [['name' => 'value', 'type' => 'integer']]
            ],
            // 触发日期之后
            'trigger_after' => [
                'name' => 'trigger_after',
                'value' => 'between',
                'validate_field' => [['name' => 'value', 'type' => 'integer']]
            ]
        ],
        // 日期类型
        'date_picker' => [
            // 多少天内
            'in_day' => [
                'name' => 'in_day',
                'value' => '<=',
                'validate_field' => [['name' => 'value', 'type' => 'integer']]
            ],
            // 指定日期
            'exact_date' => [
                'name' => 'exact_date',
                'value' => '==',
                'validate_field' => [['name' => 'value', 'type' => 'date']]
            ],
            // 指定日期之间
            'between' => [
                'name' => 'between',
                'value' => 'between',
                'validate_field' => [['name' => 'start_date', 'type' => 'date'], ['name' => 'end_date', 'type' => 'date']] //操作需要验证的字段
            ],
            // 指定日期之前
            'on_or_before' => [
                'name' => 'on_or_before',
                'value' => '<=',
                'validate_field' => [['name' => 'value', 'type' => 'date']]
            ],
            // 指定日期之后
            'on_or_after' => [
                'name' => 'on_or_after',
                'value' => '>=',
                'validate_field' => [['name' => 'value', 'type' => 'date']]
            ]
        ],
        // 数值限制
        'number_check' => [
            'greater_than' => [
                'name' => 'greater_than',
                'value' => '>',
                'validate_field' => [['name' => 'value', 'type' => 'integer']]
            ],
            'less_than' => [
                'name' => 'less_than',
                'value' => '<',
                'validate_field' => [['name' => 'value', 'type' => 'integer']]
            ],
            'equal_to' => [
                'name' => 'equal_to',
                'value' => '=',
                'validate_field' => [['name' => 'value', 'type' => 'integer']]
            ],
            'greater_or_equal_to' => [
                'name' => 'greater_or_equal_to',
                'value' => '>=',
                'validate_field' => [['name' => 'value', 'type' => 'integer']]
            ],
            'less_or_equal_to' => [
                'name' => 'less_or_equal_to',
                'value' => '<=',
                'validate_field' => [['name' => 'value', 'type' => 'integer']]
            ]
        ],

        'specific_country' => [
            'specific_country' => [
                'name' => 'specific_country',
                'value' => 'in',
                'validate_field' => [],
            ],
        ],

        'date_number_picker' => [
            'greater_than' => [
                'name' => 'greater_than',
                'value' => '>',
                'validate_field' => [['name' => 'value', 'type' => 'integer']]
            ],
            'less_than' => [
                'name' => 'less_than',
                'value' => '<',
                'validate_field' => [['name' => 'value', 'type' => 'integer']]
            ],
            'equal_to' => [
                'name' => 'equal_to',
                'value' => '=',
                'validate_field' => [['name' => 'value', 'type' => 'integer']]
            ],
            'greater_or_equal_to' => [
                'name' => 'greater_or_equal_to',
                'value' => '>=',
                'validate_field' => [['name' => 'value', 'type' => 'integer']]
            ],
            'less_or_equal_to' => [
                'name' => 'less_or_equal_to',
                'value' => '<=',
                'validate_field' => [['name' => 'value', 'type' => 'integer']]
            ]
        ],
    ],
];
