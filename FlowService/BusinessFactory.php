<?php

namespace App\Services\Flow;

use App\Enums\Flow\FlowEventHandleEnum;
use App\Lib\BaseFactory;
use App\Lib\BaseFactoryTrait;
use App\Services\Flow\Activity\CustomerTagService;
use App\Services\Flow\Activity\FlowSesService;
use App\Services\Flow\Activity\FlowSmsService;
use App\Services\Flow\Activity\OrderTagService;
use App\Services\Flow\Timer\FlowTimerService;

/**
 * 审批活动业务工厂
 * Class BusinessFactory
 * @package App\Services\Flow
 */
class BusinessFactory extends BaseFactory
{
    use BaseFactoryTrait;

    /**
     * 对象集合
     * @var array
     */
    protected static $objects = [];

    /**
     * @var string[]
     */
    protected static $map = [
        FlowEventHandleEnum::TaskDelay => FlowTimerService::class,
        FlowEventHandleEnum::OrderTags => OrderTagService::class,
        FlowEventHandleEnum::CustomerTags => CustomerTagService::class,
        FlowEventHandleEnum::SendSms => FlowSmsService::class,
        FlowEventHandleEnum::SendEmail => FlowSesService::class,
    ];
}
