<?php


namespace App\Services\Flow;


use App\Enums\Mail\MailSendMarkingTypeEnums;
use App\Enums\Mail\TemplateTypeEnums;
use App\Models\Mail\TemplateMarketingAssociation;

class FlowEmailService
{
    /**
     * @desc 添加邮件模板与flow 关联关系
     *       注意 一个flow业务流程中可以有多个店铺邮件模板一对多的情况
     * @param string $shopSign       店铺UUID
     * @param string $flowSign       店铺flow UUID
     * @param null|array $marketSign 店铺邮件模板
     * @param int $action 1、新增；2、更新；3、删除
     * @return boolean
     */
    public function bindEmailTemplate($shopSign, $flowSign, $marketSign, $action = 1)
    {
        // 先删除
        if ($action != 1) { // 若不是新增
            $this->unBindEmailTemplate($shopSign, $flowSign);
        }

        // 再新增
        $insertData = $this->formatInsertData($shopSign, $flowSign, $marketSign);
        if ($action != 3) { // 若不是删除
            $this->addBindEmailTemplate($insertData);
        }

        return true;
    }

    /**
     * @desc 解除关联
     */
    private function unBindEmailTemplate($shopSign, $flowSign)
    {
        TemplateMarketingAssociation::where(['shop_sign' => $shopSign, 'marketing_sign' => $flowSign])->delete();
        return true;
    }

    /**
     * @desc 新增关联
     */
    private function addBindEmailTemplate($insertArr)
    {
        if (!empty($insertArr)) {
            TemplateMarketingAssociation::insert($insertArr);
        }

        return true;
    }

    /**
     * @desc 组装新增关联关系数据
     * @return array
     */
    private function formatInsertData($shopSign, $flowSign, $marketSign)
    {
        $insertArr = [];
        if (is_array($marketSign) && !empty($marketSign)) {
            foreach ($marketSign as $sign) {
                $insertArr[] = [
                    'shop_sign'      => $shopSign,
                    'marketing_sign' => $flowSign,
                    'template_sign'  => $sign,
                    'marketing_type' => MailSendMarkingTypeEnums::MARKING_TYPE_FLOW,
                    'template_type'  => TemplateTypeEnums::SHOP_TYPE,
                ];
            }
        }

        return $insertArr;
    }


}
