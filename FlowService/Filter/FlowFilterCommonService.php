<?php

namespace App\Services\Flow\Filter;

use App\Enums\Flow\FlowTypeEnum;
use App\Exceptions\FlowFilterException;
use App\Models\Collects\CollectShopifyCustomerDay;
use App\Models\Collects\CollectShopifyCustomerInfo;
use App\Models\Flow\FlowOrder;
use App\Models\Segments\SegmentSmsSubscriber;
use App\Models\Shopifys\ShopifyAbandonedCheckout;
use App\Models\Shopifys\ShopifyAbandonedCheckoutVariant;
use App\Models\Shopifys\ShopifyCustomerTag;
use App\Models\Shopifys\ShopifyOrder;
use App\Models\Shopifys\ShopifyOrderVariant;

class FlowFilterCommonService
{
    // 条件
    public $filter;
    // 业务数据
    public $data;
    // flow order
    public $flowOrder;
    // 触发器
    public $type;
    // flow 数据---ple_flow_shops
    public $flow;
    // 直接sql验证
    public $directSql;

    public function __construct($filter = null, $flowOrder = null, $flow = 0, $directSql = false)
    {
        $this->data = $flowOrder['business_data'];
        $this->flowOrder = $flowOrder;
        $this->filter = $filter;
        $this->type = $flow['type'] ?? 0;
        $this->flow = $flow;
        $this->directSql = $directSql;
    }

    public function init()
    {
        $flowFilterService = \App::make(FlowFilterService::class, ['filter' => $this->filter]);
        $this->filter = $flowFilterService->getFilter();
        return $this;
    }

    /**
     * 统一调用验证
     *
     * User: pancake
     * DateTime: 2022/11/29 16:37
     * @return mixed
     * @throws FlowFilterException
     */
    public function getCheck()
    {
        $serviceName = config('flow.filters.' . $this->filter['filter_field'] . '.service');
        if (empty($serviceName)) {
            throw new FlowFilterException($this->filter['filter_field']. ' not filter service');
        }

        $service = \App::make($serviceName, ['filter' => $this->filter, 'flowOrder' => $this->flowOrder, 'flow' => $this->flow, 'directSql' => $this->directSql]);
        if (! method_exists($service, 'check')) {
            throw new FlowFilterException($serviceName.' not check function');
        }

        return $service->check();
    }

    /**
     * 获取model
     *
     * User: pancake
     * DateTime: 2022/11/29 16:39
     * @return string|void
     * @throws FlowFilterException
     */
    public function getModel()
    {
        switch ($this->type) {
            case FlowTypeEnum::Checkout:
                return $this->getCheckoutModel();
            case FlowTypeEnum::OrderPaid:
            case FlowTypeEnum::OrderCancel:
            case FlowTypeEnum::OrderFulfilled:
            case FlowTypeEnum::OrderRefunded:
                return $this->getOrderModel();
            default:
                throw new FlowFilterException($this->type . ' invalid type model');
        }
    }

    public function getCustomerModel()
    {
        switch ($this->filter['filter_field']) {
            case 'orders_count':
            case 'total_spent':
                return app(CollectShopifyCustomerDay::class);
            case 'segment':
                return app(SegmentSmsSubscriber::class);
            case 'customer_tag':
                return app(ShopifyCustomerTag::class);
            case 'customer_sms_sub_at':
            case 'specific_country':
                return app(CollectShopifyCustomerInfo::class);
            default:
                throw new FlowFilterException($this->filter['filter_field'] . ' invalid filter field customer model');
        }
    }

    public function getRuleModel()
    {
        return app(FlowOrder::class);
    }

    public function getCheckoutModel()
    {
        switch ($this->filter['filter_field']) {
            case 'brought_product':
                return app(ShopifyAbandonedCheckoutVariant::class);
            case 'collection':
                return ShopifyAbandonedCheckoutVariant::leftJoin('shopify_collection_products as scp', 'scp.product_id', 'shopify_abandoned_checkout_variants.product_id');
            case 'checkout_created_at':
                return app(ShopifyAbandonedCheckout::class);
            case 'contain_product_order_num' :
                return ShopifyOrder::join('shopify_order_variants as sov', 'sov.order_sign', 'shopify_orders.sign');
            default:
                throw new FlowFilterException($this->filter['filter_field'] . ' invalid filter field checkout model');
        }
    }

    public function getOrderModel()
    {
        switch ($this->filter['filter_field']) {
            case 'brought_product':
                return app(ShopifyOrderVariant::class);
            case 'collection':
                return ShopifyOrderVariant::leftJoin('shopify_collection_products as scp', 'scp.product_id', 'shopify_order_variants.product_id');
            case 'order_created_at':
            case 'order_cancel_reason':
                return app(ShopifyOrder::class);
            case 'contain_product_order_num' :
                return ShopifyOrder::join('shopify_order_variants as sov', 'sov.order_sign', 'shopify_orders.sign');
            default:
                throw new FlowFilterException($this->filter['filter_field'] . ' invalid filter field order model');
        }
    }

    public function modelToSql($model, $select)
    {
        $bindings = $model->getBindings();
        $sql = $model->limit(1)->select($select)->toSql();
        if (count($bindings) == 0) {
            return $sql;
        }
        $sql = str_replace(['%', '?'], ['%%', '"%s"'], $sql);
        return sprintf($sql, ...$bindings);
    }

    public function dateToInt($date)
    {
        return (Int)\Carbon\Carbon::parse($date)->format('Ymd');
    }

    public function getBusinessSignField()
    {
        switch ($this->type) {
            case FlowTypeEnum::Checkout:
                return in_array($this->filter['filter_field'], ['checkout_created_at']) ? 'sign':'checkout_sign';
            case FlowTypeEnum::OrderPaid:
            case FlowTypeEnum::OrderCancel:
            case FlowTypeEnum::OrderFulfilled:
            case FlowTypeEnum::OrderRefunded:
                return in_array($this->filter['filter_field'], ['order_created_at', 'order_cancel_reason']) ? 'sign':'order_sign';
            default:
                throw new FlowFilterException($this->type . ' invalid type business sign');
        }
    }

    /**
     * 包含器
     *
     * User: pancake
     * DateTime: 2022/11/29 16:37
     * @param array $checkArr 验证数据
     * @param string $operation 操作
     * @param array $valArr 过滤数据
     * @return bool
     */
    public function containPicker($checkArr, $operation, $valArr)
    {
        $dataLen = count(array_unique($checkArr));
        $diffLen = count(array_unique(array_diff($checkArr, $valArr)));
        return ($operation == 'in' && $diffLen != $dataLen) || ($operation == 'not in' && $diffLen == $dataLen);
    }

    public function datePicker($date, $isDirect = true, $datePicker = 'date_picker')
    {
        $startDate = $endDate = null;
        switch ($this->filter[$datePicker]['name']) {
            case 'in_day':
                $endDate = date('Y-m-d', strtotime('+' . $this->filter[$datePicker]['value'] . ' day', strtotime($this->flow['created_at'])));
                break;
            case 'exact_date':
                $startDate = $endDate = date('Y-m-d', strtotime($this->filter[$datePicker]['value']));
                break;
            case 'between':
                $startDate = date('Y-m-d', strtotime($this->filter[$datePicker]['start_date']));
                $endDate = date('Y-m-d', strtotime($this->filter[$datePicker]['end_date']));
                break;
            case 'on_or_before':
                $endDate = date('Y-m-d', strtotime($this->filter[$datePicker]['value']));
                break;
            case 'on_or_after':
                $startDate = date('Y-m-d', strtotime($this->filter[$datePicker]['value']));
                break;
            case 'trigger_after':
                $startDate = date('Y-m-d', strtotime('+1 day', strtotime($this->flowOrder['created_at'])));
                $endDate = date('Y-m-d', strtotime('+' . $this->filter[$datePicker]['value'] . ' day', strtotime($this->flowOrder['created_at'])));
                break;
            case 'trigger_before':
                $startDate = date('Y-m-d', strtotime('-' . $this->filter[$datePicker]['value'] . ' day', strtotime($this->flowOrder['created_at'])));
                $endDate = date('Y-m-d', strtotime('-1 day', strtotime($this->flowOrder['created_at'])));
                break;
            default :
                return false;
        }

        if (!$isDirect) {
            return ['startDate' => $startDate, 'endDate' => $endDate];
        }

        return (!empty($startDate) && $startDate > $date) || (!empty($endDate) && $endDate < $date) ? false : true;
    }
}
