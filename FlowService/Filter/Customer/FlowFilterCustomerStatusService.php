<?php

namespace App\Services\Flow\Filter\Customer;
use App\Services\Flow\Filter\FlowFilterCommonService;

/**
 * Class FlowFilterSegmentService
 *
 * 校验顾客是否为某个状态
 * @package App\Services\Flow\Filter
 */
class FlowFilterCustomerStatusService extends FlowFilterCommonService
{
    public function __construct($filter = null, $flowOrder = null, $flow = null, $directSql = false)
    {
        parent::__construct($filter, $flowOrder, $flow, $directSql);
    }

    public function check()
    {
        // 直接判断
        if (empty($this->data['customer_info']['customer_state'])) {
            return false;
        }

        return $this->containPicker([$this->data['customer_info']['customer_state']], $this->filter['contain_picker']['operation'], $this->filter['contain_picker']['value']);

    }
}
