<?php

namespace App\Services\Flow\Filter\Customer;
use App\Services\Flow\Filter\FlowFilterCommonService;
use Illuminate\Support\Facades\DB;

/**
 * Class FlowFilterBroughtProductService
 *
 * 校验顾客订单数量
 * @package App\Services\Flow\Filter
 */
class FlowFilterOrderCountService extends FlowFilterCommonService
{
    public function __construct($filter = null, $flowOrder = null, $flow = 0, $directSql = false)
    {
        parent::__construct($filter, $flowOrder, $flow, $directSql);
    }

    public function check()
    {
        if ($this->directSql) {
            // model  直接验证
            return $this->modelCheck();
        } else {
            // sql 语句
            return $this->sqlCheck();
        }
    }

    public function modelCheck($isSql = false)
    {
        $model = $this->getCustomerModel();

        $model = $model->where('customer_id', $this->data['customer_id']);
        $dateArr = $this->datePicker(null, false, 'customer_date_picker');

        if (!empty($dateArr['startDate'])) {
            $model = $model->where('date', '>=', $this->dateToInt($dateArr['startDate']));
        }
        if (!empty($dateArr['endDate'])) {
            $model = $model->where('date', '<=', $this->dateToInt($dateArr['endDate']));
        }

        if ($isSql) {
            return $this->modelToSql($model, DB::raw('ifnull(sum(number_of_orders), 0) as number_of_orders'));
        }

        $orderCount = $model->sum('number_of_orders');
        $operation = ($this->filter['number_check']['operation'] == '=' ? '=':'').$this->filter['number_check']['operation'];
        $value = $this->filter['number_check']['value'];
        eval("\$flag = $orderCount $operation $value;");

        return $flag;
    }

    public function sqlCheck()
    {
        $sql = $this->modelCheck(true);
        return '('.$sql.') '.$this->filter['number_check']['operation'].$this->filter['number_check']['value'];
    }
}
