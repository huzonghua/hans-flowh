<?php

namespace App\Services\Flow\Filter\Customer;
use App\Services\Flow\Filter\FlowFilterCommonService;

/**
 * Class FlowFilterBroughtProductService
 *
 * 校验顾客是否为某个分类内
 * @package App\Services\Flow\Filter
 */
class FlowFilterSegmentService extends FlowFilterCommonService
{
    public function __construct($filter = null, $flowOrder = null, $flow = null, $directSql = false)
    {
        parent::__construct($filter, $flowOrder, $flow, $directSql);
    }

    public function check()
    {
        // 判断业务数据是否携带判断数据
        if (!empty($this->data['customer_info']['segment_signs']) && is_array($this->data['customer_info']['segment_signs'])) {
            // 直接判断
            return $this->directCheck();
        } else if ($this->directSql) {
            // model  直接验证
            return $this->modelCheck();
        } else {
            // sql 语句
            return $this->sqlCheck();
        }
    }

    public function directCheck(): bool
    {
        return $this->containPicker($this->data['customer_info']['segment_signs'], $this->filter['contain_picker']['operation'], $this->filter['contain_picker']['value']);
    }

    public function modelCheck($isSql = false)
    {
        $model = $this->getCustomerModel();

        $model = $model->where('customer_id', $this->data['customer_id']);
        if ($this->filter['contain_picker']['operation'] == 'in') {
            $model = $model->whereIn('segment_sign', $this->filter['contain_picker']['value']);
        } else {
            $model = $model->whereNotIn('segment_sign', $this->filter['contain_picker']['value']);
        }

        if ($isSql) {
            return $this->modelToSql($model, 'customer_id');
        }

        return $model->exists() ? true : false;
    }

    public function sqlCheck()
    {
        $sql = $this->modelCheck(true);
        return '('.$sql.') is not null';
    }
}
