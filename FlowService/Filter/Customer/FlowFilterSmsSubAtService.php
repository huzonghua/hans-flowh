<?php

namespace App\Services\Flow\Filter\Customer;
use App\Services\Flow\Filter\FlowFilterCommonService;

/**
 * Class FlowFilterSmsSubAtService
 *
 * 校验顾客的短信订阅时间
 * @package App\Services\Flow\Filter
 */
class FlowFilterSmsSubAtService extends FlowFilterCommonService
{
    public function __construct($filter = null, $flowOrder = null, $flow = null, $directSql = false)
    {
        parent::__construct($filter, $flowOrder, $flow, $directSql);
    }

    public function check()
    {
        // 判断业务数据是否携带判断数据
        if (!empty($this->data['customer_info']['consent_updated_at'])) {
            // 直接判断
            return $this->directCheck();
        } else if ($this->directSql) {
            // model  直接验证
            return $this->modelCheck();
        } else {
            // sql 语句
            return $this->sqlCheck();
        }
    }

    public function directCheck(): bool
    {
        return $this->datePicker(date('Y-m-d', strtotime($this->data['customer_info']['consent_updated_at'])), true, 'customer_date_picker');
    }

    public function modelCheck($isSql = false)
    {
        $model = $this->getCustomerModel();
        $orderSign = 'customer_id';

        $model = $model->where($orderSign, $this->data['customer_id']);
        $dateArr = $this->datePicker(null, false, 'customer_date_picker');

        if (!empty($dateArr['startDate'])) {
            $model = $model->where('consent_updated_at', '>=', $this->dateToInt($dateArr['startDate']));
        }
        if (!empty($dateArr['endDate'])) {
            $model = $model->where('consent_updated_at', '<=', $this->dateToInt($dateArr['startDate']));
        }

        if ($isSql) {
            return $this->modelToSql($model, $orderSign);
        }

        return $model->exists() ? true : false;
    }

    public function sqlCheck()
    {
        $sql = $this->modelCheck(true);
        return '('.$sql.') is not null';
    }
}
