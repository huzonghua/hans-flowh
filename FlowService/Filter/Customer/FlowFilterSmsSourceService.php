<?php

namespace App\Services\Flow\Filter\Customer;
use App\Enums\Subscriber\SystemSubscriptionStatusEnum;
use App\Services\Flow\Filter\FlowFilterCommonService;

/**
 * Class FlowFilterBroughtProductService
 *
 * 校验用户订阅渠道
 * @package App\Services\Flow\Filter
 */
class FlowFilterSmsSourceService extends FlowFilterCommonService
{
    public function __construct($filter = null, $flowOrder = null, $flow = null, $directSql = false)
    {
        parent::__construct($filter, $flowOrder, $flow, $directSql);
    }

    public function check()
    {
        // 直接判断，未订阅 直接返回false
        if ((empty($this->data['customer_info']['consent_collected_from']) || $this->data['customer_info']['sms_subscription_status'] != SystemSubscriptionStatusEnum::Subscribed) &&
            (empty($this->data['customer_info']['email_consent_collected_from']) || $this->data['customer_info']['email_subscription_status'] != SystemSubscriptionStatusEnum::Subscribed)) {
            return false;
        }

        return $this->containPicker([$this->data['customer_info']['consent_collected_from']], $this->filter['contain_picker']['operation'], $this->filter['contain_picker']['value']) ||
            $this->containPicker([$this->data['customer_info']['email_consent_collected_from']], $this->filter['contain_picker']['operation'], $this->filter['contain_picker']['value']);
    }
}
