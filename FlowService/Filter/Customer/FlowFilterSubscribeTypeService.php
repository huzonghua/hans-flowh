<?php

namespace App\Services\Flow\Filter\Customer;
use App\Enums\Subscriber\SystemSubscriptionStatusEnum;
use App\Services\Flow\Filter\FlowFilterCommonService;

/**
 * Class FlowFilterSegmentService
 *
 * 校验用户是否订阅 （订阅包括 邮件订阅或者短信订阅）
 * @package App\Services\Flow\Filter
 */
class FlowFilterSubscribeTypeService extends FlowFilterCommonService
{
    public function __construct($filter = null, $flowOrder = null, $flow = null, $directSql = false)
    {
        parent::__construct($filter, $flowOrder, $flow, $directSql);
    }

    public function check()
    {
        // 直接判断
        if ($this->filter['contain_picker']['value'] == 0) { // 全部用户 无需判断
            return true;
        }

        return $this->containPicker($this->getSubscriptType(), $this->filter['contain_picker']['operation'], $this->filter['contain_picker']['value']);
    }

    /**
     * @desc 获取当前触发用户的订阅类型
     * @return array
     */
    private function getSubscriptType()
    {
        // 若当前用户有一个订阅（邮件/短信） 则是 订阅
        if ($this->data['customer_info']['sms_subscription_status'] == SystemSubscriptionStatusEnum::Subscribed ||
            $this->data['customer_info']['email_subscription_status'] == SystemSubscriptionStatusEnum::Subscribed) { // 订阅状态
            $currentSubscription = [
                SystemSubscriptionStatusEnum::Subscribed
            ];
        } else { // 非订阅（退订和未订阅）
            $currentSubscription = [
                SystemSubscriptionStatusEnum::NotSubscribed,
                SystemSubscriptionStatusEnum::UnSubscribed
            ];
        }

        return $currentSubscription;
    }


}
