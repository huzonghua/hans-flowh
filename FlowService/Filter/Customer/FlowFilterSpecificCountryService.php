<?php

namespace App\Services\Flow\Filter\Customer;
use App\Services\Flow\Filter\FlowFilterCommonService;

/**
 * Class FlowFilterSegmentService
 *
 * 校验顾客所属地
 * @package App\Services\Flow\Filter
 */
class FlowFilterSpecificCountryService extends FlowFilterCommonService
{
    public function __construct($filter = null, $flowOrder = null, $flow = null, $directSql = false)
    {
        parent::__construct($filter, $flowOrder, $flow, $directSql);
    }

    public function check()
    {
        // 判断业务数据是否携带判断数据
        if (isset($this->data['customer_info']['country_code_v2']) && isset($this->data['customer_info']['province_code'])) {
            // 直接判断
            return $this->directCheck();
        } else if ($this->directSql) {
            // model  直接验证
            return $this->modelCheck();
        } else {
            // sql 语句
            return $this->sqlCheck();
        }
    }

    public function directCheck(): bool
    {
        $countries = $this->filter['specific_country'];
        foreach ($countries as $country) {
            if ($country['country_code'] == $this->data['customer_info']['country_code_v2'] &&
                (empty($country['province_code']) || in_array($this->data['customer_info']['province_code'], $country['province_code']))) {
                return true;
            }
        }
        return false;
    }

    public function modelCheck($isSql = false)
    {
        $model = $this->getCustomerModel();

        $model = $model->where('customer_id', $this->data['customer_id']);

        $countries = $this->filter['specific_country'];
        $model = provinceCollection($countries, $model);

        if ($isSql) {
            return $this->modelToSql($model, 'customer_id');
        }

        return $model->exists() ? true : false;
    }

    public function sqlCheck()
    {
        $sql = $this->modelCheck(true);
        return '('.$sql.') is not null';
    }
}
