<?php

namespace App\Services\Flow\Filter\Customer;
use App\Services\Flow\Filter\FlowFilterCommonService;

/**
 * Class FlowFilterBroughtProductService
 *
 * 校验用户最后一单的时间
 * @package App\Services\Flow\Filter
 */
class FlowFilterLastOrderAtService extends FlowFilterCommonService
{
    public function __construct($filter = null, $flowOrder = null, $flow = null, $directSql = false)
    {
        parent::__construct($filter, $flowOrder, $flow, $directSql);
    }

    public function check()
    {
        // 直接判断，未下单直接false
        if (empty($this->data['customer_info']['last_order_at'])) {
            return false;
        }

        $operation = ($this->filter['number_check']['operation'] == '=' ? '=':'').$this->filter['number_check']['operation'];
        $day = (strtotime(date('Y-m-d')) - strtotime(date('Y-m-d', strtotime($this->data['customer_info']['last_order_at']))))/(3600*24);
        $value = $this->filter['number_check']['value'];

        eval("\$flag = $day $operation $value;");

        return $flag;
    }
}
