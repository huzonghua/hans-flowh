<?php

namespace App\Services\Flow\Filter\Order;
use App\Services\Flow\Filter\FlowFilterCommonService;

/**
 * Class FlowFilterBroughtProductService
 *
 * 校验业务数据是否存在或者不存在某产品
 * @package App\Services\Flow\Filter
 */
class FlowFilterBroughtProductService extends FlowFilterCommonService
{
    public function __construct($filter = null, $flowOrder = null, $flow = 0, $directSql = false)
    {
        parent::__construct($filter, $flowOrder, $flow, $directSql);
    }

    public function check()
    {
        // 判断业务数据是否携带 产品id数组
        if (!empty($this->data['product_ids']) && is_array($this->data['product_ids'])) {
            // 直接判断
            return $this->directCheck();
        } else if ($this->directSql) {
            // model  直接验证
            return $this->modelCheck();
        } else {
            // sql 语句
            return $this->sqlCheck();
        }
    }

    public function directCheck(): bool
    {
        return $this->containPicker($this->data['product_ids'], $this->filter['contain_picker']['operation'], $this->filter['contain_picker']['value']);
    }

    public function modelCheck($isSql = false)
    {
        $model = $this->getModel();
        $orderSign = $this->getBusinessSignField();

        $model = $model->where($orderSign, $this->data['order_sign']);
        if ($this->filter['contain_picker']['operation'] == 'in') {
            $model = $model->whereIn('product_id', $this->filter['contain_picker']['value']);
        } else {
            $model = $model->whereNotIn('product_id', $this->filter['contain_picker']['value']);
        }

        if ($isSql) {
            return $this->modelToSql($model, $orderSign);
        }

        return $model->exists() ? true : false;
    }

    public function sqlCheck()
    {
        $sql = $this->modelCheck(true);
        return '('.$sql.') is not null';
    }
}
