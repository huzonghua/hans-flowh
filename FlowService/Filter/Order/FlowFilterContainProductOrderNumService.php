<?php

namespace App\Services\Flow\Filter\Order;
use App\Enums\Shopify\OrderFinancialStatusEnum;
use App\Services\Flow\Filter\FlowFilterCommonService;
use Illuminate\Support\Facades\DB;

/**
 * Class FlowFilterBroughtProductService
 *
 * 校验当前用户购买某个产品的订单统计
 * @package App\Services\Flow\Filter
 */
class FlowFilterContainProductOrderNumService extends FlowFilterCommonService
{
    public function __construct($filter = null, $flowOrder = null, $flow = 0, $directSql = false)
    {
        parent::__construct($filter, $flowOrder, $flow, $directSql);
    }

    public function check()
    {
        // 判断业务数据是否携带 产品id数组
        if (!empty($this->data['product_ids']) && is_array($this->data['product_ids'])) {
            // 直接判断
            return $this->directCheck();
        } else if ($this->directSql) {
            // model  直接验证
            return $this->modelCheck();
        } else {
            // sql 语句
            return $this->sqlCheck();
        }
    }

    public function directCheck(): bool
    {
        return $this->containPicker($this->data['product_ids'], $this->filter['contain_picker']['operation'], $this->filter['contain_picker']['value']);
    }

    /**
     * @desc 已经支付的订单 关联指定商品处理 包含当前订单
     * @param false $isSql
     * @return string
     * @throws \App\Exceptions\FlowFilterException
     */
    public function modelCheck($isSql = false)
    {
        $model = $this->getModel();
        $dateArr = $this->datePicker(null, false, 'customer_date_picker');

        $model->where('shopify_orders.shop_sign', $this->data['shop_sign']);
        $model->whereIn('shopify_orders.financial_status', [OrderFinancialStatusEnum::Paid, OrderFinancialStatusEnum::Refunded, OrderFinancialStatusEnum::PartiallyRefunded]);
        $model->where('shopify_orders.customer_id', $this->data['customer_id']);
        if ($this->filter['contain_picker']['operation'] == 'in') {
            $model->whereIn('sov.product_id', $this->filter['contain_picker']['value']);
        } else {
            $model->whereNotIn('sov.product_id', $this->filter['contain_picker']['value']);
        }

        if (!empty($dateArr['startDate'])) {
            $model->where('shopify_orders.shopify_created_at', '>=', str_replace(' ', 'T', $dateArr['startDate'] . ' 00:00:00Z'));
        }
        if (!empty($dateArr['endDate'])) {
            $model->where('shopify_orders.shopify_created_at', '<=', str_replace(' ', 'T', $dateArr['endDate'] . ' 00:00:00Z'));
        }

        if ($isSql) {
            return $this->modelToSql($model, DB::raw('count(DISTINCT(ple_shopify_orders.id)) as number_of_orders'));
        }
        $orderCount = $model->distinct()->count('shopify_orders.id');
        $operation = ($this->filter['number_check']['operation'] == '=' ? '=':'').$this->filter['number_check']['operation'];
        $value = $this->filter['number_check']['value'];

        eval("\$flag = $orderCount $operation $value;");

        return $flag;
    }

    public function sqlCheck()
    {
        $sql = $this->modelCheck(true);
        return '('.$sql.') '.$this->filter['number_check']['operation'].$this->filter['number_check']['value'];
    }
}
