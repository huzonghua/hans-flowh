<?php

namespace App\Services\Flow\Filter\Order;

use App\Services\Flow\Filter\FlowFilterCommonService;

/**
 * Class FlowFilterBroughtProductService
 *
 * 校验业务数据是否存在或者不存在某系列
 * @package App\Services\Flow\Filter
 */
class FlowFilterOrderCancelReasonService extends FlowFilterCommonService
{
    public function __construct($filter = null, $flowOrder = null, $flow = null, $directSql = false)
    {
        parent::__construct($filter, $flowOrder, $flow, $directSql);
    }

    public function check()
    {
        // 判断业务数据是否携带判断数据
        if (!empty($this->data['cancel_reason'])) {
            // 直接判断
            return $this->directCheck();
        } else if ($this->directSql) {
            // model  直接验证
            return $this->modelCheck();
        } else {
            // sql 语句
            return $this->sqlCheck();
        }
    }

    public function directCheck(): bool
    {
        return $this->containPicker([$this->data['cancel_reason']], $this->filter['contain_picker']['operation'], $this->filter['contain_picker']['value']);
    }

    public function modelCheck($isSql = false)
    {
        $model = $this->getModel();
        $orderSign = $this->getBusinessSignField();

        $model = $model->where($orderSign, $this->data['order_sign']);
        if ($this->filter['contain_picker']['operation'] == 'in') {
            $model = $model->whereIn('cancel_reason', $this->filter['contain_picker']['value']);
        } else {
            $model = $model->whereNotIn('cancel_reason', $this->filter['contain_picker']['value']);
        }

        if ($isSql) {
            return $this->modelToSql($model, $orderSign);
        }

        return $model->exists() ? true : false;
    }

    public function sqlCheck()
    {
        $sql = $this->modelCheck(true);
        return '('.$sql.') is not null';
    }
}
