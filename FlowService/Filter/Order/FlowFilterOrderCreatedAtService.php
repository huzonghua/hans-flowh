<?php

namespace App\Services\Flow\Filter\Order;
use App\Models\Shopifys\ShopifyOrder;
use App\Services\Flow\Filter\FlowFilterCommonService;

/**
 * Class FlowFilterBroughtProductService
 *
 * 校验订单创建时间
 * @package App\Services\Flow\Filter
 */
class FlowFilterOrderCreatedAtService extends FlowFilterCommonService
{
    public function __construct($filter = null, $flowOrder = null, $flow = 0, $directSql = false)
    {
        parent::__construct($filter, $flowOrder, $flow, $directSql);
    }

    public function check()
    {
        // 判断业务数据是否携带判断数据
        if (!empty($this->data['created_at'])) {
            // 直接判断
            return $this->directCheck();
        } else if ($this->directSql) {
            // model  直接验证
            return $this->modelCheck();
        } else {
            // sql 语句
            return $this->sqlCheck();
        }
    }

    public function directCheck(): bool
    {
        return $this->datePicker(date('Y-m-d', strtotime($this->data['created_at'])));
    }

    public function modelCheck($isSql = false)
    {
        $model = $this->getModel();
        $orderSign = $this->getBusinessSignField();

        $model = $model->where($orderSign, $this->data['order_sign']);
        $dateArr = $this->datePicker(null, false);

        if (!empty($dateArr['startDate'])) {
            $model = $model->where('created_at', '>=', $dateArr['startDate'].' 00:00:00');
        }
        if (!empty($dateArr['endDate'])) {
            $model = $model->where('created_at', '<=', $dateArr['endDate'].' 23:59:59');
        }

        if ($isSql) {
            return $this->modelToSql($model, $orderSign);
        }

        return $model->exists() ? true : false;
    }

    public function sqlCheck()
    {
        $sql = $this->modelCheck(true);
        return '('.$sql.') is not null';
    }
}
