<?php

namespace App\Services\Flow\Filter\Rule;
use App\Enums\Flow\FlowOrderStatusEnum;
use App\Services\Flow\Filter\FlowFilterCommonService;

/**
 * Class FlowFilterRuleService
 *
 * 校验触发规则
 * @package App\Services\Flow\Filter
 */
class FlowFilterRuleService extends FlowFilterCommonService
{
    public function __construct($filter = null, $flowOrder = null, $flow = 0, $directSql = false)
    {
        parent::__construct($filter, $flowOrder, $flow, $directSql);
    }

    public function check()
    {
        if (empty($this->filter['number_check']['value'])) {
            // 时间为空时，直接返回true
            return in_array($this->filter['filter_field'], ['trigger_not_event_day', 'flow_not_event_day']);
        }

        if ($this->directSql) {
            // model  直接验证
            return $this->modelCheck();
        } else {
            // sql 语句
            return $this->sqlCheck();
        }
    }

    public function modelCheck($isSql = false)
    {
        $model = $this->getRuleModel();

        $model = $model->where('customer_id', $this->data['customer_id']);
        $groupByFields = ['customer_id'];

        switch ($this->filter['filter_field']) {
            // 触发器
            case 'trigger_event_num':
            case 'trigger_not_event_day':
                array_push($groupByFields, 'type');
                $model = $model->where('type', $this->flow['type']);
                break;
            // 流
            case 'flow_event_num':
            case 'flow_not_event_day':
                array_push($groupByFields, 'flow_shop_id');
                $model = $model->where('flow_shop_id', $this->flow['id']);
                break;
        }

        switch ($this->filter['filter_field']) {
            // 触发次数
            case 'trigger_event_num': // 全局触发器以订单为统计维度累计次数
                $model = $model->groupBy($groupByFields)
                    ->havingRaw('count(DISTINCT(business_id)) >='.$this->filter['number_check']['value']);
                break;
            case 'flow_event_num':
                $model = $model->groupBy($groupByFields)
                    ->havingRaw('count(*) >='.$this->filter['number_check']['value']);
                break;
            // 触发时间
            case 'trigger_not_event_day':
            case 'flow_not_event_day':
                $model = $model->where('created_at', '>=', date('Y-m-d H:i:s', strtotime('-'.($this->filter['number_check']['value']).' hour')));
                break;
        }

        $model = $model->where('sign', '<>', $this->flowOrder['sign'])
            ->where('flow_status', '<>', FlowOrderStatusEnum::RULE_FAIL);

        if ($isSql) {
            return $this->modelToSql($model, 'customer_id');
        }

        return $model->exists() ? false : true;
    }

    public function sqlCheck()
    {
        $sql = $this->modelCheck(true);
        return '('.$sql.') is null';
    }
}
