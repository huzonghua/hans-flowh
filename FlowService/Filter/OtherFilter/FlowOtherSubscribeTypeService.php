<?php


namespace App\Services\Flow\Filter\OtherFilter;


use App\Enums\Flow\FlowEventEnum;
use App\Enums\Flow\FlowFilterEnum;
use App\Enums\Subscriber\SystemSubscriptionStatusEnum;
use App\Lib\Code;

class FlowOtherSubscribeTypeService
{
    /**
     * @desc 触发器层级增加 选择可以触发短信|邮件给 订阅或者非订阅用户 的处理条件
     * 若是非订阅则会有特殊得处理流程
     * 1、添加订阅和非订阅用户是否可以触发Flow
     * 2、非订阅Flow中邮件、短信只能用在弃单场景，其他场景禁用
     * 3、非订阅48小时只能发一次短信，邮件不受限制（需要核查定时器等是否满足时间差）
     * 4、短信标签和订单标签不受限制 但非订阅用户的标签无需自动同步
     * 5、非订阅用户条件过滤节点均可无需处理
     * 6、若是弃单则 FLOW 规则 时差不能小于24小时
     * 此处处理1、5
     * @param $nodes
     * @return mixed
     */
    public function mergeFilter(&$nodes)
    {
        $flag = isset($nodes[0]['subscriber_type']);
        if (!$flag) { // 若未设置 无需处理 兼容历史规则
            return true;
        }
        $isSubscribe = $nodes[0]['subscriber_type'] == SystemSubscriptionStatusEnum::Subscribed; // 发送对象是否是订阅
        foreach ($nodes as &$node) {
            // Flow触发规则 添加订阅和非订阅用户是否可以触发Flow
            if ($flag && $node['event'] == FlowEventEnum::StartConditional) {
                // 添加订阅与否限制规则
                $this->handleRule($node);
                // 非订阅需要限制频次（订阅无需处理）
                if (!$isSubscribe) {
                    $this->checkTimeGap($node['rule']);
                }
            }

            // 非订阅用户条件过滤节点均可无需处理
            if (!$isSubscribe && $node['event'] == FlowEventEnum::ConditionalEvent) {
                $node['filter'] = [];
            }
        }
    }

    /**
     * @desc 新增flow规则级别的用户订阅类型规则限制 （对用户不可见）
     * @param $node
     */
    private function handleRule(&$node)
    {
        $subscriberTypeFilter = [
            'filter_field'=> 'customer_subscriber_type',
            'contain_picker' =>  [
                'name' => 'contains',
                'value' => [$node['subscriber_type'] ?? 0]
            ]
        ];

        // 数组中一维元素间是或的关系，每个一维元素数组下的子数组是且的关系
        if (empty($node['filter'])) {
            $node['filter'][0] = [$subscriberTypeFilter];
        } else {
            array_push($node['filter'][0], $subscriberTypeFilter);
        }

        // unset($node['subscriber_type']);
    }

    /**
     * @desc 检查时间间隔
     * @param $rules
     * @throws \Exception
     */
    private function checkTimeGap($rules)
    {
        foreach ($rules as $rule) {
            foreach ($rule as $row) {
                if ($row['filter_field'] == FlowFilterEnum::FlowNotEventDay) {
                   if ($row['number_check']['value'] < 24) {
                       throw new \Exception(__('filter.flow_time_limit_checkout'), Code::COMMON_TIP);
                   }
                   break;
                }
            }
        }
    }


}
