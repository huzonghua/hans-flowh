<?php


namespace App\Services\Flow\Filter\OtherFilter;


use App\Enums\Flow\FlowEventEnum;

class FlowOtherOrderProductService
{
    /**
     * @desc 针对[购买|没购买]指定商品订单数的组合过滤条件
     *     仅针对条件判断节点且同一个过滤条件组下 同时含有 订单和产品 限制 有效
     *    （此处仅后端逻辑处理需要 前端无需调整）
     * @param $nodes
     * @return mixed
     */
    public function mergeFilter(&$nodes)
    {
        foreach ($nodes as &$node) {
            if ($node['event'] == FlowEventEnum::ConditionalEvent) {
                foreach ($node['filter'] as &$row) { // 遍历条件组 $row 间是 or 关系 ，$row 内的条件是 and 关系
                    $filterItem = array_column($row, null, 'filter_field');
                    if (isset($filterItem['orders_count'], $filterItem['brought_product'])) {
                        // 将且关系的 订单和产品过滤条件 替换成新的处理逻辑 contain_product_order_num 配置
                        unset($filterItem['orders_count']['filter_field'], $filterItem['brought_product']['filter_field']);
                        $filterItem['contain_product_order_num'] = [
                            'filter_field' => 'contain_product_order_num',
                        ];
                        // 合并过滤条件
                        $filterItem['contain_product_order_num'] = array_merge($filterItem['contain_product_order_num'], $filterItem['orders_count'], $filterItem['brought_product']);
                        // 去掉旧条件
                        unset($filterItem['orders_count'], $filterItem['brought_product']);
                        $row = array_values($filterItem);
                    }
                }
            }
        }
    }


}
