<?php

namespace App\Services\Flow\Filter;

use App\Services\Flow\Filter\OtherFilter\FlowOtherOrderProductService;
use App\Services\Flow\Filter\OtherFilter\FlowOtherSubscribeTypeService;

/**
 * flow业务流程节点其他需要必须校验的过滤节点处理工厂
 */
class FlowOtherFilterFactory
{
    /**
     * @var string[]
     */
    public static $map = [
         FlowOtherOrderProductService::class,
         FlowOtherSubscribeTypeService::class,
    ];


}
