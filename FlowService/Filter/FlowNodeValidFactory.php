<?php

namespace App\Services\Flow\Filter;

use App\Enums\Flow\FlowEventHandleEnum;
use App\Lib\BaseFactory;
use App\Lib\BaseFactoryTrait;
use App\Services\Flow\Filter\Node\FlowNodeSesValidService;
use App\Services\Flow\Filter\Node\FlowNodeSmsValidService;
use App\Services\Flow\Filter\Node\FlowNodeTagValidService;

/**
 * flow创建业务流程节点属性验证工厂
 */
class FlowNodeValidFactory extends BaseFactory
{
    use BaseFactoryTrait;

    /**
     * 对象集合
     * @var array
     */
    protected static $objects = [];

    /**
     * @var string[]
     */
    protected static $map = [
        FlowEventHandleEnum::SendSms => FlowNodeSmsValidService::class,
        FlowEventHandleEnum::SendEmail => FlowNodeSesValidService::class,
        FlowEventHandleEnum::CustomerTags => FlowNodeTagValidService::class,
        FlowEventHandleEnum::OrderTags => FlowNodeTagValidService::class,
    ];
}
