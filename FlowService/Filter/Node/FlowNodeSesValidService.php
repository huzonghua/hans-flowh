<?php


namespace App\Services\Flow\Filter\Node;


use App\Enums\Mail\TemplateTypeEnums;
use App\Enums\Shopify\DiscountRuleTypeEnum;
use App\Enums\System\SmsIsDiscountEnum;
use App\Services\Mail\MailService;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Support\Facades\Validator;

class FlowNodeSesValidService extends FlowNodeValidBaseService
{
    public function validNode(&$node)
    {
        // 基础变量校验
        Validator::make($node['data'], [
            'send_subject' => 'required|string',
            'sender_mail' => 'required|email',
            'sender_name' => 'required|string',
            'reply_mail' => 'required|email',
            'email_template_sign' => 'required|string',
            'is_discount_rules' => ['required', 'integer', new EnumValue(SmsIsDiscountEnum::class)],
            'discount_rules_data' => 'required_if:is_discount_rules,1|array',
            'discount_rules_data.rules_type_name' => ['required_if:is_discount_rules,1', 'string', new EnumValue(DiscountRuleTypeEnum::class)],
            'discount_rules_data.rules_value' => 'required_if:is_discount_rules,1',
            'discount_rules_data.ends_at' => 'required_if:is_discount_rules,1|integer',
        ])->validate();

        // 只存取有效变量
        $node['data'] = [
            'send_subject' => $node['data']['send_subject'],
            'sender_mail' => $node['data']['sender_mail'],
            'sender_name' => $node['data']['sender_name'],
            'reply_mail' => $node['data']['reply_mail'],
            'email_template_sign' => $node['data']['email_template_sign'],
            'is_discount_rules' => $node['data']['is_discount_rules'],
            'discount_rules_data' => $node['data']['discount_rules_data']
        ];

        // 店铺邮件模板
        if (!empty($node['data']['email_template_sign'])) {
            app(MailService::class)->checkTemplates($node['data']['email_template_sign'], TemplateTypeEnums::SHOP_TYPE);
        }
    }


}
