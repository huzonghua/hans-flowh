<?php


namespace App\Services\Flow\Filter\Node;


interface FlowNodeValidInterface
{
    /**
     * @desc 验证节点提交业务参数是否符合要求
     * @param array $node
     * @return mixed
     */
    public function validNode(&$node);
}
