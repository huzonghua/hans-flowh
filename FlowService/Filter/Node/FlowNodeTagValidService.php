<?php


namespace App\Services\Flow\Filter\Node;


use Illuminate\Support\Facades\Validator;

class FlowNodeTagValidService extends FlowNodeValidBaseService
{
    public function validNode(&$node)
    {
        Validator::make($node, [
            'data.*' => ['required', 'string'],
        ])->validate();
    }


}
