<?php


namespace App\Services\Flow\Filter\Node;


use App\Enums\Merchant\MerchantPlanPermissionNameEnum;
use App\Enums\Merchant\MerchantShopAuthStatusEnum;
use App\Enums\Shopify\DiscountRuleTypeEnum;
use App\Enums\System\SmsIsDiscountEnum;
use App\Exceptions\MerchantCenter\UpgradePackageException;
use App\Lib\Code;
use App\Models\MerchantCenter\MerchantShops;
use App\Services\Keyword\ShopKeywordService;
use App\Services\SmsBusiness\SensitiveWords;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Support\Facades\Validator;

class FlowNodeSmsValidService extends FlowNodeValidBaseService
{
    public function validNode(&$node)
    {
        // 发送短信
        Validator::make($node['data'], [
            'sms_content_html' => ['required'],
            'is_discount_rules' => ['required', 'integer', new EnumValue(SmsIsDiscountEnum::class)],
            'discount_rules_data' => 'required_if:is_discount_rules,1|array',
            'discount_rules_data.rules_type_name' => ['required_if:is_discount_rules,1', 'string', new EnumValue(DiscountRuleTypeEnum::class)],
            'discount_rules_data.rules_value' => 'required_if:is_discount_rules,1',
            'discount_rules_data.ends_at' => 'required_if:is_discount_rules,1|integer',
        ])->validate();
        // 处理url值中的不同字符比如各国文字等内容
        if (!empty($node['data']['discount_rules_data'])) {
            $node['data']['discount_rules_data'] = discountUrlValueEncode($node['data']['discount_rules_data']);
        }

        $node['data']['sms_content'] = filter_template_html($node['data']['sms_content_html']);
        unset($node['data']['sms_content_html']);
        // 敏感词检查
        app(SensitiveWords::class)->checkSensitiveWords($node['data']['sms_content']);
        // 结账页变量
        if (strrpos($node['data']['sms_content'], '{ProductPurchaseUrl}') !== false) {
            if (!hasPermission(MerchantPlanPermissionNameEnum::PersonalRecommend)) {
                throw new UpgradePackageException('');
            }
            if (empty($node['data']['product_purchase_data']) || !is_array($node['data']['product_purchase_data'])) {
                throw new \Exception('flow send sms product_purchase_data empty', Code::COMMON_TIP);
            }

            Validator::make($node['data']['product_purchase_data'], [
                'is_code' => ['required', 'integer', 'in:0,1'],
                'variants' => ['required', 'array'],
                'variants.*.id' => ['required'],
                'variants.*.num' => ['required', 'integer'],
            ])->validate();
            $shop = MerchantShops::where('merchant_id', $this->merchantId)
                ->where('shop_sign', $this->shopSign)
                ->where('auth_status', MerchantShopAuthStatusEnum::Success)
                ->first(['shop_url']);
            $node['data']['product_purchase_data']['shop_url'] = $shop->shop_url ?? '';
        }

        // 短信模板
        if (!empty($node['data']['sms_template_sign'])) {
            app(ShopKeywordService::class)->smsTemplateExists($node['data']['sms_template_sign']);
        }
    }


}
