<?php

namespace App\Services\Flow\Filter;

use App\Exceptions\FlowFilterException;

class FlowFilterService
{
    // 条件
    public $filter;

    public function __construct($filter = null)
    {
        $this->filter = $filter;
        // 数据验证
        $this->valid();
    }

    public function setFilter($filter)
    {
        $this->filter = $filter;
        // 数据验证
        $this->valid();
    }

    public function getFilter()
    {
        return $this->filter;
    }

    private function valid()
    {
        // 数据验证
        $this->validFilter();
        $this->validFilterField();
        $this->validOperation();
        $this->validPicker();
    }

    /**
     * 验证条件格式数据不能为空
     *
     * User: pancake
     * DateTime: 2022/11/25 10:31
     * @throws FlowFilterException
     */
    private function validFilter()
    {
        $fields = ['filter_field'];
        $this->validFieldEmpty($this->filter, $fields);
    }

    /**
     * 验证过滤字段是否存在
     *
     * User: pancake
     * DateTime: 2022/11/25 10:31
     * @throws FlowFilterException
     */
    private function validFilterField()
    {
        if (!in_array($this->filter['filter_field'], array_keys(config('flow.filters')))) {
            throw new FlowFilterException('filter_field: ' . $this->filter['filter_field'].' invalid');
        }
    }

    /**
     * 验证操作选择器的数据
     *
     * User: pancake
     * DateTime: 2022/11/25 14:44
     * @throws FlowFilterException
     */
    private function validOperation()
    {
        $operations = config('flow.filters.'.$this->filter['filter_field'].'.operation');
        $this->filter['operations'] = $operations;
        foreach ($operations as $operation) {
            if (empty($this->filter[$operation])) {
                throw new FlowFilterException(__($this->filter['filter_field'].'  filter.'.$operation.'_empty'));
            }
        }
    }

    private function validFieldEmpty($data, $fields)
    {
        foreach ($fields as $field) {
            if (isset($data[$field]) && is_numeric($data[$field])) {
                // 数字可以为0
                continue;
            }
            if (empty($data[$field])) {
                // 字符串 不能为空
                throw new FlowFilterException(__('filter.'.$field.'_empty'));
            }
        }
    }

    private function validFieldTypeEmpty($data, $fields)
    {
        foreach ($fields as $field) {
            if (!(isset($data[$field['name']]) && is_numeric($data[$field['name']])) && empty($data[$field['name']])) {
                // 字符串 不能为空
                throw new FlowFilterException(__('filter.'.$field['name'].'_empty'));
            }

            // 判断输入的类型
            if (!empty($field['type']) && $field['type'] == 'date' && date('Y-m-d', strtotime($data[$field['name']])) != $data[$field['name']]) {
                throw new FlowFilterException($field['name'] . ' field type : '.$field['type']);
            } else if (!empty($field['type']) && $field['type'] != 'date'  && gettype($data[$field['name']]) != $field['type']) {
                throw new FlowFilterException($field['name'] . ' field type : '.$field['type']);
            }
        }
    }

    /**
     * 验证选择器的数据
     *
     * User: pancake
     * DateTime: 2022/11/25 14:43
     * @throws FlowFilterException
     */
    private function validPicker()
    {
        foreach ($this->filter['operations'] as $operation) {
            if ($operation == 'specific_country') {
                // 国家，特殊处理
                $this->validCountry();
                continue;
            }
            $pickers = config('flow.operations.'.$operation);

            if (empty($this->filter[$operation]['name'])) {
                throw new FlowFilterException($operation. ' name  invalid');
            }

            if (! in_array($this->filter[$operation]['name'], array_keys($pickers))) {
                throw new FlowFilterException($operation. ' name: ' . $this->filter['filter_field'].' invalid');
            }
            $picker = $pickers[$this->filter[$operation]['name']];
            $this->validFieldTypeEmpty($this->filter[$operation], $picker['validate_field'] ?? []);
            $this->filter[$operation]['operation'] = $picker['value'];
        }
    }

    /**
     * 验证国家
     *
     * User: pancake
     * DateTime: 2022/11/25 14:43
     * @throws FlowFilterException
     */
    private function validCountry()
    {
        foreach ($this->filter['specific_country'] as $country) {
            if (empty($country['country_code'])) {
                throw new FlowFilterException('.specific_country '. __('filter.specific_country_c_empty'));
            }

            if (!empty($country['province_code']) && !is_array($country['province_code'])) {
                throw new FlowFilterException('specific_country.province_code field type is: array');
            }
        }
    }
}
