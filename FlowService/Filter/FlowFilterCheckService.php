<?php

namespace App\Services\Flow\Filter;

use Illuminate\Support\Facades\DB;

class FlowFilterCheckService
{
    public array $filterResult = [];
    /**
     * 验证 flow 节点的条件是否成立
     *
     * User: pancake
     * DateTime: 2022/12/2 15:03
     * @param array $filters
     * @param array $flow 无需rule字段
     * @param array $flowOrder
     * @param false $isCheckFilterField 是否校验条件的字段格式
     * @return bool
     */
    public function checkFilters($filters, $flow, $flowOrder, $isCheckFilterField = false)
    {
        $this->filterResult = []; // 记录过滤过程和结果
        $orSql = '';
        $isQuerySql = false;
        foreach ($filters as $filterAnds) {
            $sql = '';
            foreach ($filterAnds as $filter) {
                $flowFilterCommonService = app(FlowFilterCommonService::class, ['filter' => $filter, 'flow' => $flow, 'flowOrder' => $flowOrder]);
                if ($isCheckFilterField) {
                    $flowFilterCommonService = $flowFilterCommonService->init();
                }

                $check = $flowFilterCommonService->getCheck();
                array_push($this->filterResult, [$filter['filter_field'] => $check]); // 记录单个过滤条件结果
                if ($check === false) {
                    // 当 and 条件其中一个为 false 时，直接跳过
                    $sql = ' 1=0 ';
                    break;
                } else if ($check !== true) {
                    $sql .= ($sql == '' ? '' : ' and ') . $check;
                    $isQuerySql = true;
                }
            }

            // 当某个 or 条件成立，直接返回true
            if (empty($sql)) {
                return true;
            }

            $orSql .= ($orSql == '' ? '' : ' or ') . '('.$sql.')';
        }

        // 当没有条件时，直接返回true
        if (empty($orSql)) {
            return true;
        }

        // 当所有or条件都返回 false，直接返回false
        if (!$isQuerySql) {
            return false;
        }

        $checkSql = 'select ('.$orSql.') as flag';
        $checkResult = DB::selectOne($checkSql)->flag == 1;
        // 记录当前条件节点全部过滤过程和结果
        array_push($this->filterResult, ['node_whole_filter' => $checkSql, 'filter_result' => $checkResult]);

        return $checkResult;
    }
}
