<?php

namespace App\Services\Flow\Log;

use App\Enums\Mail\MailSendMarkingTypeEnums;
use App\Enums\System\SmsServiceTypeEnum;
use App\Http\Resources\Flow\FlowOrderLogListResource;
use App\Models\CommonModel;
use Illuminate\Support\Facades\DB;

class FlowShopOrderEventService
{
    /**
     * @desc 获取flow触发记录执行节点日志
     * @param $data
     * @param $merchantUser
     * @return FlowOrderLogListResource
     */
    public function getList($data, $merchantUser)
    {
        $perPage = $data['limit'] ?? CommonModel::PAGE_SIZE;
        $sms = DB::table('sms_content')
            ->selectRaw('sign, 1 as event_type, total_price as cost_price, phone as target_subject,
            marketing_type as flow_type, e_send_time as send_time, send_status, result')
            ->whereIn('shop_sign', $merchantUser->shop_signs)
            ->where('marketing_sign', $data['flow_sign'])
            ->where('marketing_type', SmsServiceTypeEnum::Flow);

        $mail = DB::table('mail_send')
            ->selectRaw('sign, 2 as event_type,mail_price as cost_price, receive_mail as target_subject,
            marketing_type as flow_type, ifnull(send_time, created_at) as send_time, send_status, result')
            ->whereIn('shop_sign', $merchantUser->shop_signs)
            ->where('marking_sign', $data['flow_sign'])
            ->where('marketing_type', MailSendMarkingTypeEnums::MARKING_TYPE_FLOW);

        $data = $sms->union($mail)->orderBy('send_time', 'desc')->paginate($perPage);

        return new FlowOrderLogListResource($data);
    }


}
