<?php

namespace App\Services\Flow\Log;

use App\Enums\Flow\FlowShopStatusEnum;
use App\Enums\Flow\FlowTypeEnum;
use App\Models\CommonModel;
use App\Models\Flow\FlowShop;

class FlowShopBusinessService
{
    /**
     * @desc 获取flow的业务访问汇总详情
     * @param $sign
     * @param $merchantUser
     * @return array
     * @throws \Exception
     */
    public function getFlowBusinessDetail($sign, $merchantUser)
    {
        $flowInfo = FlowShop::where('sign', $sign)->whereIn('shop_sign', $merchantUser->shop_signs)->first();

        return $this->formatSendInfo($flowInfo);
    }

    /**
     * @desc 格式化flow发送详情内容
     * @param $flowInfo
     * @return array
     */
    private function formatSendInfo($flowInfo)
    {
        return ['sign' => $flowInfo->sign ?? '',
                'name' => $flowInfo->name ?? '',
                'type' => $flowInfo->type ?? '',
                'type_txt' => FlowTypeEnum::getDescription($flowInfo->type),
                'status' => $flowInfo->status,
                'send_user_num' => $flowInfo->send_user_num,
                'shop_sign' => $flowInfo->shop_sign ?? '',
                'send_num_info' => [
                    'send_num_total' => $flowInfo->send_num + $flowInfo->sms_send_num,
                    'sms_send_num' => $flowInfo->sms_send_num,
                    'email_send_num' => $flowInfo->send_num
                ],
                'click_num_info' => [
                    'send_num_total' => $flowInfo->sms_click_num + $flowInfo->click_num,
                    'sms_click_num' => $flowInfo->sms_click_num,
                    'email_click_num' => $flowInfo->click_num,
                ],
                'income_info' => [
                    'income_total' => getIncome($flowInfo->income + $flowInfo->sms_income),
                    'sms_income'   => getIncome($flowInfo->sms_income),
                    'email_income' => getIncome($flowInfo->income),
                ],
                'cost_info' =>  [
                    'cost_total' =>  ($flowInfo->sms_cost + $flowInfo->cost)/ CommonModel::AMOUNT_MULTIPLE,
                    'sms_cost' => $flowInfo->sms_cost / CommonModel::AMOUNT_MULTIPLE,
                    'email_cost' => $flowInfo->cost / CommonModel::AMOUNT_MULTIPLE,
                ],
                'status_text'  => FlowShopStatusEnum::getDescription($flowInfo->status),
                'approve_tips' => isset(FlowShopStatusEnum::$approveTips[$flowInfo->status]) ? __('tips.' . FlowShopStatusEnum::$approveTips[$flowInfo->status]) : '',
        ];
    }


}
