<?php


namespace App\Services\Flow;


use App\Models\Flow\FlowOrdersLog;
use Plexins\Flow\Tool\FlowStorageInterface;

class FlowStorageService implements FlowStorageInterface
{
    public function addLog($flowInfo = [])
    {
        $model = new FlowOrdersLog();
        $model->flow_order_id = $flowInfo['flow_order_id'];
        $model->node = (string)$flowInfo['node'];
        $model->prv_node = (string)$flowInfo['prv_node'];
        $model->event = (string)$flowInfo['event'];
        $model->filter_param = (string)$flowInfo['filter_param'];
        $model->filter_result = (string)$flowInfo['filter_result'];
        $model->flow_batch_sign = (string)$flowInfo['flow_batch_sign'];// execute batch sign
        $model->handler = (string)$flowInfo['handler'];
        $model->save();
    }


}
