<?php


namespace App\Services\Flow;


use App\Models\Flow\FlowTask;
use Illuminate\Support\Facades\DB;

class FlowTaskService
{
    public $retryNum = 1; // 任务执行失败 默认重试次数

    public function __construct($retryNum = 1)
    {
        $this->retryNum = $retryNum;
    }

    /**
     * @desc  获取flow 业务单据状态
     * 注意此处更新为完成需要全部的异步任务都完成才可以更新完成否则不能
     * @param $task
     * @param $flowStatusCode
     * @return int|mixed
     */
    public function filterFlowOrderStatus($task, $flowStatusCode)
    {
        $exist = FlowTask::where(['flow_order_id' => $task['flow_order_id'], 'task_type' => FlowTask::TIMER_TASK])
            ->whereIn('status', [FlowTask::WAITING])
            ->where('id', '!=', $task['id'])
            ->first(['id']);
        return $flowStatusCode == 200 && !empty($exist) ? 100 : $flowStatusCode;
    }

    /**
     * @desc 更新延迟任务状态
     * @param $id
     * @param $flowStatusCode
     * @param $remark
     * @return mixed
     */
    public function updateFlowTaskStatus($id, $flowStatusCode, $remark = '')
    {
        $updateColumns = [
            'num' => DB::raw("num + 1"),
            'status' => $flowStatusCode == 200 ? FlowTask::SUCCESS : FlowTask::FAIL
        ];
        if ($flowStatusCode == 400) { // 重试不更新状态
            $updateColumns['status'] = FlowTask::DOING;
        }
        if (!empty($remark)) {
            $updateColumns['remark'] = $remark;
        }

        return FlowTask::where(['id' => $id])->update($updateColumns);
    }

    /**
     * @param $shopSign
     * @param $pageSize
     * @param $taskType
     * @param int $thread
     * @return array
     */
    public function getNeedTodo($shopSign, $taskType, $status, $pageSize, $currentThread = -1, $totalThread = 0, $flowSign = '')
    {
        $query = FlowTask::query()
            ->select('id', 'flow_order_id', 'shop_sign', 'run_at', 'node', 'remark', 'status', 'num', 'task_type', 'request_data', 'created_at')
            ->with(['withFlowOrder'])
            ->whereIn('status' , $status)
            ->where('run_at', '<=', date('Y-m-d H:i:s'))
            ->where(['task_type' => $taskType])
            ->where('num', '<=', $this->retryNum);
        if ($shopSign) {
            $query->whereIn('shop_sign', $shopSign);
        }
        if (!empty($flowSign)) {
            $query->where('flow_sign', $flowSign);
        }
        if ($totalThread > 0 && $currentThread >= 0 && $totalThread > $currentThread) {
            $query->where(DB::raw('id%'.$totalThread), '=', $currentThread);
        }
        return $query->limit($pageSize)
            ->orderBy('status')
            ->orderBy('num')
            ->orderBy('updated_at')
            ->get()->toArray();
    }

    /**
     * @desc 因多个流程交叉 执行而终止当前流 比如
     * 弃单触发后，用户又进行了支付操作，则 弃单后续流程终止.
     * @param $flowOrderIds
     * @return mixed
     */
    public function terminalTask($flowOrderIds)
    {
        return FlowTask::whereIn('flow_order_id', $flowOrderIds)->whereIn('status', [FlowTask::WAITING, FlowTask::FAIL, FlowTask::DOING])
            ->where('num', '<=', $this->retryNum)
            ->update(['status' => FlowTask::SUCCESS, 'remark' => DB::raw("CONCAT(remark, ' flow terminal.')")]);
    }


}
