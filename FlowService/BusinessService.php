<?php


namespace App\Services\Flow;


use App\Lib\Code;
use App\Services\Flow\Filter\FlowFilterCheckService;
use Plexins\Flow\Tool\BusinessInterface;
use Plexins\Flow\Tool\FlowStorageInterface;


class BusinessService implements BusinessInterface
{
    public $model = null;
    public $storage = null;

    public function __construct(FlowStorageInterface $flowStorage, $model = '')
    {
         $this->model = $model;
         $this->storage = $flowStorage;
    }

    public function handle($nodeInfo, $parentId = '')
    {
        $isRule = 0;                                     // 是否是触发器规则
        $filterName = [];                                // 响应结果存储处理

        $this->model['currentNode'] = $nodeInfo;         // 注入当前处理节点对象
        $this->model['parentNodeId'] = $parentId;        // 记录父级节点ID
        $result = true;
        $flowFilterCheck = new FlowFilterCheckService(); // 过滤检查服务类

        // 全局触发器规则。只校验第一次，通过则后续无需再次校验，若不通过则此触发器相关流程全部不再执行
        if (isset($nodeInfo->rule['common_trigger_rule'])) {
            $this->commonTriggerRuleFilter($nodeInfo, $flowFilterCheck);
            unset($nodeInfo->rule['common_trigger_rule']);
        }

        try {
            // 流触发规则
            if (isset($nodeInfo->rule) && !empty($nodeInfo->rule)) {
                $tmpResult = $flowFilterCheck->checkFilters($nodeInfo->rule, $this->model['business_data']['flow'], $this->model, true);
                $filterName = $flowFilterCheck->filterResult;
                if (!$tmpResult) {
                    throw new \Exception('trigger rule check failure,flow exit.', Code::FLOW_RULE_ERROR);
                }
                $this->addLog(['result' => true, 'filterName' => $filterName, 'rule' => 1]);
            }

            // 条件分支逻辑处理
            if ($nodeInfo->handle == 'filterParse') {
                $result = $flowFilterCheck->checkFilters($nodeInfo->filter, $this->model['business_data']['flow'], $this->model, true);
                $filterName = $flowFilterCheck->filterResult;
            } else {  // 节点动作处理 包括行动、定时任务
                if (isset($nodeInfo->handle) && !empty($nodeInfo->handle)) {
                    list($result, $filterName) = BusinessFactory::getObject($nodeInfo->handle)->handle($this->model);
                    $filterName = ['handle_type' => $nodeInfo->handle, 'detail_info' => $filterName];
                }
            }
        } catch (\Exception $e) {
            $result = false;
            if ($e->getCode() == Code::FLOW_RULE_ERROR) {
                $isRule = 1;
            }
            $filterName[] = $e->getMessage();
        }

        // 记录当前节点执行日志
        $this->addLog(['result' => $result, 'filterName' => $filterName, 'rule' => $isRule]);

        return $result;
    }

    /**
     * 保存执行结果日志记录
     * @param array $params
     * @return bool
     */
    public function addLog($params)
    {
        $filterParam = isset($this->model['currentNode']->filter) ? $this->model['currentNode']->filter : '';
        if (isset($params['rule']) && $params['rule'] >= 1) {
            $filterParam = $this->model['currentNode']->rule;
        }

        $logInfo = [
            'prv_node' => (string)$this->model['parentNodeId'],
            'node' => $this->model['currentNode']->id,
            'handle' => $this->model['currentNode']->handle,
            'event' => $this->model['currentNode']->event,
            'filter_param' => json_encode($filterParam),
            'filter_result' => json_encode($params),
            'flow_order_id' => $this->model['flow_order_id'],
            'flow_batch_sign' => $this->model['business_data']['flow_batch_sign'],
            'handler' => isset($this->model['handler']) ? $this->model['handler'] : 'webhoook',
        ];

        $this->storage->addLog($logInfo);

        return true;
    }

    /**
     * @desc handle the flow result
     * @param $flowOrderId
     * @param $flowStatusCode
     * @param string $currentNodeId
     */
    public function handleFlowResult($flowOrderId, $flowStatusCode, $currentNodeId = '')
    {
        return app(FlowOrderService::class)->updateFlowOrderStatus($flowOrderId, $flowStatusCode, $currentNodeId);
    }

    /**
     * @desc 全局触发器规则过滤
     * @param object $nodeInfo
     * @param object $flowFilterCheck
     * @throws \Exception
     */
    public function commonTriggerRuleFilter($nodeInfo, $flowFilterCheck)
    {
        if (!empty($nodeInfo->rule['common_trigger_rule'])) {
            $tmpResult = $flowFilterCheck->checkFilters([$nodeInfo->rule['common_trigger_rule']], $this->model['business_data']['flow'], $this->model, true);
            $filterName = $flowFilterCheck->filterResult;
            if (!$tmpResult) {
                $filterName[] = 'The trigger rule conditions are not met.';
                $this->addLog(['result' => false, 'filterName' => $filterName, 'rule' => 2]);
                throw new \Exception('The trigger rule conditions are not met.', Code::FLOW_GLOB_TRIGGER_RULE_ERROR);
            }
            $this->addLog(['result' => true, 'filterName' => $filterName, 'rule' => 2]);
        }

        return true;
    }



}
