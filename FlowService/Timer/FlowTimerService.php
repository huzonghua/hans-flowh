<?php


namespace App\Services\Flow\Timer;


use App\Enums\Flow\FlowOrderStatusEnum;
use App\Enums\Flow\FlowTypeEnum;
use App\Models\Flow\FlowTask;
use App\Services\DotRecordService;
use App\Services\Flow\BusinessService;
use App\Services\Flow\FlowStorageService;
use App\Services\Flow\FlowTaskService;
use Illuminate\Support\Facades\Redis;
use Plexins\Flow\Engine;
use Plexins\Flow\Flow;

class FlowTimerService
{
    /**
     * @desc flow 节点中 延迟节点处理逻辑入口
     *  最终会生成异步待执行的任务。
     * @param $requestData
     * @return array
     */
    public function handle($requestData)
    {
        try {
            $result = true; // 注意此值 当前只有单分支 若有多分支则需权衡什么情况下可以继续后续流程
            $msg = '';
            $currentNode = $requestData['currentNode'];
            unset($requestData['currentNode']);
            $targetSeconds = time() + intval($currentNode->sleep); // 计时器开始时间是以当前事件触发时间 + 定时时间(秒)，多个定时器串行追加
            foreach ($currentNode->out as $node) {
                $flowTask = new FlowTask();
                $flowTask->flow_order_id = $requestData['flow_order_id'];
                $flowTask->shop_sign = $requestData['shop_sign'];
                $flowTask->run_at = date('Y-m-d H:i:s', $targetSeconds);
                $flowTask->node = $node->id;
                $flowTask->task_type = FlowTask::TIMER_TASK;
                $flowTask->request_data = ['prvNodeId' => $currentNode->id];
                $flowTask->flow_sign = $requestData['business_data']['flow']['sign'] ?? '';
                $result = $flowTask->save();
            }
        } catch (\Exception $e) {
            $msg = $e->getMessage().PHP_EOL.$e->getTraceAsString();
            $result = false;
        }

        return [$result, $msg];
    }

    /**
     * @desc 定时任务执行延迟执行的后续的流程
     * @param $shopSign
     * @param $pageSize
     * @param int $currentThread
     * @param int $totalThread
     */
    public function handleDelayTask($shopSign, $pageSize, $currentThread, $totalThread)
    {
        $flowLogService = new FlowStorageService();
        $flowTaskService = new FlowTaskService();

        $waitHandleArr = $flowTaskService->getNeedTodo($shopSign,
            FlowTask::TIMER_TASK,
            [FlowTask::WAITING, FlowTask::FAIL],
            $pageSize,
            $currentThread,
            $totalThread
        );

        foreach ($waitHandleArr as $task) {
            try {
                // combine data
                $requestData = $this->formatData($task);
                // 消费数据开始打点
                $dotData= [
                    'action' => $requestData['business_data']['action'],
                    'dotRecordTypeId' => $requestData['business_data']['dot_record_type_id'],
                    'webhookData' => $requestData,
                    'status' => 'RUNNING',
                    'exception' => '',
                ];
                unset($task['with_flow_order']);
                if (in_array($requestData['flow_status'], FlowOrderStatusEnum::$doneStatusMap)) {
                    $flowTaskService->updateFlowTaskStatus($task['id'], 200);
                    continue;
                }
                $flowRule = $requestData['business_data']['flow']['rule'] ?? '';
                if (empty($flowRule)) {
                    throw new \Exception('flow rule empty.'.json_encode($requestData));
                }
                $this->saveDot($dotData);
                // init
                $business = new BusinessService($flowLogService, $requestData);
                $flowParse = new Flow($flowRule, $task['node']);

                // start flow
                $engine = new Engine($flowParse, $business);
                $flowStatusCode = $engine->start($requestData['prvNodeId']);

                // handle result
                $flowStatusCode = $flowStatusCode == 100 ? 200 : $flowStatusCode;
                $result = $flowTaskService->updateFlowTaskStatus($task['id'], $flowStatusCode);

                if ($result) {
                    $flowStatusCode = $flowTaskService->filterFlowOrderStatus($task, $flowStatusCode);
                    $business->handleFlowResult($requestData['flow_order_id'], $flowStatusCode, $engine->currentNodeId);
                }
                $dotData['status'] = 'SUCCESS';
            } catch (\Exception $e) {
                $dotData['status'] = 'FAIL';
                $dotData['exception'] = $e;
            }

            // 消费数据结束打点
            $this->saveDot($dotData);
        }
    }

    /**
     * @desc 组装延迟任务所需的业务参数
     * @param $task
     * @return array
     */
    public function formatData($task)
    {
        $requestData = $task['with_flow_order'];
        $shopInfo = getShopInfo($requestData['shop_sign']);
        $requestData['flow_order_id'] = $requestData['id'];
        $requestData = array_merge($requestData, $task['request_data']);
        if (empty($shopInfo)) { // 若店铺无效则规则也失效
            $requestData['flow_status'] = FlowOrderStatusEnum::RULE_INVALID;
        }

        return $requestData;
    }

    /**
     * 保存执行记录 注意日志记录在 0 库
     * @param $data
     * @throws \Exception
     */
    public function saveDot($data)
    {
        Redis::select(0);
        DotRecordService::addRecord('flow-timer-'. (FlowTypeEnum::$topicMap[$data['action']] ?? ''), $data['dotRecordTypeId'], $data['webhookData'], $data['status'], $data['exception']);
    }

}
