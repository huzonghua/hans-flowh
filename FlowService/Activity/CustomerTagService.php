<?php


namespace App\Services\Flow\Activity;


use App\Jobs\Flow\FlowCustomerTag;
use App\Models\Flow\FlowTask;
use App\Models\Shopifys\ShopifyCustomerTag;
use App\Services\Flow\FlowTaskService;
use App\Services\Shopify\CustomerService;
use Illuminate\Support\Facades\DB;

class CustomerTagService extends FlowTagBase
{
    /**
     * flow node handle interface.
     * @param $requestData
     * @return array
     */
    public function handle($requestDataIni)
    {
        try {
            // update customer tags
            $errorMsg = '';
            $requestData = $requestDataIni['business_data'];
            $requestData['flow_order_id'] = $requestDataIni['flow_order_id'];
            $requestData['currentNode'] = $requestDataIni['currentNode'];

            $this->requestData = $requestData;
            $customerTags = $this->getCustomerTag();
            // create sync tag task to shopify
            $result = !empty($customerTags) ? $this->createSyncTagTask(FlowTask::CUSTOMER_TAG_TASK, $customerTags) : true;
        } catch (\Exception $e) {
            $result = false;
            $errorMsg = $e->getMessage().PHP_EOL.$e->getTraceAsString();
        }

        return [$result, $errorMsg];
    }

    /**
     * update main business  info.
     * @return array
     * @throws \Exception
     */
    private function getCustomerTag()
    {
        $tags = $this->requestData['currentNode']->data;
        $setTags = is_array($tags) ? $tags : explode(',', $tags);

        return ['setTags' => $setTags,
                'customer_id' => $this->requestData['customer_id'],
                'customer_sign' => $this->requestData['customer_info']['customer_sign'] ?? '',
            ];
    }

    /**
     * execute sync task.
     * tags to platform
     * @param $shopSign
     * @param $pageSize
     * @param int $currentThread
     * @param int $totalThread
     * @return int
     */
    public function doSyncToPlatform($shopSign, $pageSize, $currentThread = -1, $totalThread = 0)
    {
        $this->flowTaskService = new FlowTaskService();
        $this->taskType = FlowTask::CUSTOMER_TAG_TASK;
        $waitHandles = $this->getWaitDoTasks($shopSign, $pageSize, $currentThread, $totalThread);

        foreach($waitHandles as $task) {
            $shop = $this->checkShop($task['shop_sign']);
            if (!$shop) { // 若店铺不可用（禁用、卸载等）
                $status = 200;
                $remark = 'shop invalid.'.$task['shop_sign'];
            } else {
                $this->tagKey = 'flow:tag:update:customer:'.$task['shop_sign'].':'.$task['request_data']['customer_id'];
                $isLock = $this->syncUpdateCache();
                if (!$isLock) {
                    continue;
                }
                list($status, $remark) = $this->platformCustomerTag($shop, $task);
            }
            // handle result
            DB::transaction(function() use($task, $status, $remark) {
                $this->updateTaskMainStatus($task, $status, $remark);
            });
        }

        return true;
    }

    /**
     * @desc 平台用户标签
     * @param $shop
     * @param $task
     * @return array
     */
    private function platformCustomerTag($shop, $task)
    {
        // 获取标签内容
        $tags = $this->getTagContents($task);
        if (empty($tags)) {
            return [200, 'Same tags, no need to synchronize'];
        }

        // 更新业务表标签
        $insertResult = $this->updateCustomerTag($tags, $task);
        if ($insertResult !== true) { // 若业务单据标签未更新成功 等待重试
            return [400, 'update customer tag error, will be retry'];
        }

        // 同步标签到平台
        isRequestRest($shop->sign);
        incrementRestRequest($shop->sign);
        $shopifyCustomerRest = new \Plexins\MyShopify\ShopifyCustomer($shop->shop_url, $shop->access_token);
        $platformResult = $shopifyCustomerRest->updateCustomerTags($task['request_data']['customer_id'], $tags);

        return $this->handleSyncTagResult($platformResult);
    }

    /**
     * @desc 获取待同步标签内容
     * @param $task
     * @return array
     */
    private function getTagContents($task)
    {
        if (!isset($task['request_data']['setTags'])) { // 兼容旧数据
            return $task['request_data']['tags'];
        }

        $where = [
            'shop_sign' => $task['shop_sign'],
            'customer_id' => $task['request_data']['customer_id'],
        ];
        $customerTags = ShopifyCustomerTag::select('name')->where($where)->get()->pluck('name')->toArray();
        $diffTags = array_diff($task['request_data']['setTags'], $customerTags);

        return empty($diffTags) ? [] : array_merge($customerTags, $diffTags);
    }

    /**
     * @desc 更新用户标签
     * @param array $diffTags
     * @param array $taskInfo
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    private function updateCustomerTag($diffTags, $taskInfo)
    {
        $data = ['tags' => $diffTags, 'sign' => $taskInfo['request_data']['customer_sign']];
        $shop = (object)['sign' => $taskInfo['shop_sign']];
        $formatTags = app()->make(CustomerService::class)->formatTags($data, $shop, $taskInfo['request_data']['customer_id']);
        $result = '';
        if (!empty($formatTags)) {
            $result = DB::transaction(function () use ($taskInfo, $formatTags) {
                ShopifyCustomerTag::where(['customer_id' => $taskInfo['request_data']['customer_id'], 'shop_sign' => $taskInfo['shop_sign']])->delete();
                ShopifyCustomerTag::insert($formatTags);
                return true;
            });
        }

        return $result;
    }


}
