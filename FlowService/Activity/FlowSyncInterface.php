<?php


namespace App\Services\Flow\Activity;


interface FlowSyncInterface
{
    // flow处理流程入口
    public function handle($request);

    // 统计发送数据 邮件、短信 发送次数 、人数、
    public function sumSendNum($shopSign, $flowSign, $customerId);

    // 异步任务处理
    public function doSyncToPlatform($shopSign, $pageSize, $currentThread = -1, $totalThread = 0);

    // 更新异步任务表单据的状态
    public function updateTaskMainStatus($task, $status, $remark);
}
