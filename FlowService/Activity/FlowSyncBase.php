<?php


namespace App\Services\Flow\Activity;


use App\Models\Flow\FlowCustomer;
use App\Models\Flow\FlowShop;
use App\Models\Flow\FlowTask;
use App\Services\Flow\FlowTaskService;
use Illuminate\Support\Facades\DB;

class FlowSyncBase implements FlowSyncInterface
{
    public array $taskWaitStatus = [FlowTask::WAITING, FlowTask::FAIL];

    public FlowTaskService $flowTaskService;

    public $taskType = -1;

    public $flowSign = '';
    /**
     * @desc flow 流程处理入口方法
     * @param $request
     */
    public function handle($request)
    {
        // TODO: Implement handle() method.
    }

    /**
     * @desc 异步定时任务处理
     * @param $shopSign
     * @param $pageSize
     * @param int $currentThread
     * @param int $totalThread
     */
    public function doSyncToPlatform($shopSign, $pageSize, $currentThread = -1, $totalThread = 0)
    {
        // TODO: Implement doSyncToPlatform() method.
    }

    /**
     * @desc 获取待处理异步任务内容
     * @param $shopSign
     * @param $pageSize
     * @param int $currentThread
     * @param int $totalThread
     * @return array
     */
    public function getWaitDoTasks($shopSign, $pageSize, $currentThread, $totalThread)
    {
        return $this->flowTaskService->getNeedTodo($shopSign, $this->taskType, $this->taskWaitStatus, $pageSize, $currentThread, $totalThread, $this->flowSign);
    }

    /**
     * @desc 更新异步任务单据状态
     * @param $task
     * @param $status
     * @param $remark
     * @param $flowOrderService
     * @return mixed
     */
    public function updateTaskMainStatus($task, $status, $remark)
    {
        return $this->flowTaskService->updateFlowTaskStatus($task['id'], $status, $remark);
    }

    /**
     * @desc 统计短信、邮件发送总数
     *      包括发送数量统计 + 发送人统计去重（用户+店铺+流程类型）
     *      !!!注意 调用方需发起事务！！！
     * @param $shopSign
     * @param $flowSign
     * @param $customerId
     * @return boolean
     */
    public function sumSendNum($shopSign, $flowSign, $customerId) :bool
    {
        $updateColumn = [];
        if (!empty($this->sendNumField)) {
            $updateColumn[$this->sendNumField] = DB::raw($this->sendNumField." + 1"); // 短信发送次数(区分邮件和 短信 )
        }

        $columns = ['shop_sign' => $shopSign,
            'flow_shop_sign' => $flowSign,
            'customer_id' => $customerId
        ];

        $updateColumn = $this->setSendCustomerAndNum($columns, $updateColumn);
        if (!empty($updateColumn)) {
            FlowShop::where(['sign' => $flowSign, 'shop_sign' => $shopSign])->update($updateColumn);
        }

        return true;
    }

    /**
     * @desc 发送人数和统计字段更新
     * @param $columns 一个用户在一个活动中只算一次
     * @param $updateColumn
     * @return array
     */
    private function setSendCustomerAndNum($columns, $updateColumn)
    {
        $customerInfo = FlowCustomer::where($columns)->first();
        if (!$customerInfo) {
            FlowCustomer::create($columns);
            $updateColumn['send_user_num'] = DB::raw("send_user_num + 1");
        }

        return $updateColumn;
    }


}
