<?php


namespace App\Services\Flow\Activity;

use App\Models\Flow\FlowTask;
use App\Models\Shopifys\ShopifyOrder;
use App\Services\Flow\FlowTaskService;
use Illuminate\Support\Facades\DB;

class OrderTagService extends FlowTagBase
{
    /**
     * flow node handle interface.
     * @param array $requestData
     * @return array
     */
    public function handle($requestDataIni)
    {
        try {
            // update order tags
            $errorMsg = '';
            $requestData = $requestDataIni['business_data'];
            $requestData['flow_order_id'] = $requestDataIni['flow_order_id'];
            $requestData['currentNode'] = $requestDataIni['currentNode'];

            $this->requestData = $requestData;
            $result = true;
            if (isset($this->requestData['order_sign'])) {
                $orderTags = $this->getOrderTag();
                // create sync tag task to shopify
                $result = !empty($orderTags) ? $this->createSyncTagTask(FlowTask::ORDER_TAG_TASK, $orderTags) : true;
            }
        } catch (\Exception $e) {
            $result = false;
            $errorMsg = $e->getMessage().PHP_EOL.$e->getTraceAsString();
        }

        return [$result, $errorMsg];
    }

    /**
     * @desc update main business  info.
     * @return array
     * @throws \Exception
     */
    private function getOrderTag()
    {
        $tags = $this->requestData['currentNode']->data;
        $setTags = is_array($tags) ? $tags : explode(',', $tags);

        $orderInfo = ShopifyOrder::where([
                'shop_sign' => $this->requestData['shop_sign'],
                'sign' => $this->requestData['order_sign'],
                'customer_id' => $this->requestData['customer_id'],
            ])
            ->first(['id', 'legacy_resource_id']);

        if (empty($orderInfo)) {
            throw new \Exception('platform order not exist.');
        }

        return ['legacy_resource_id' => $orderInfo->legacy_resource_id,
            'customer_id' => $this->requestData['customer_id'],
            'setTags' => $setTags
        ];
     }

    /**
     * @desc 定时任务处理异步任务
     * @param $shopSign
     * @param $pageSize
     * @param int $currentThread
     * @param int $totalThread
     * @return bool
     */
    public function doSyncToPlatform($shopSign, $pageSize, $currentThread = -1, $totalThread = 0)
    {
        $this->flowTaskService = new FlowTaskService();

        $this->taskType = FlowTask::ORDER_TAG_TASK;
        $waitHandles = $this->getWaitDoTasks($shopSign, $pageSize, $currentThread, $totalThread);

        foreach($waitHandles as $key => $task) {
            $shop = $this->checkShop($task['shop_sign']);
            if (!$shop) { // 若店铺不可用（禁用、卸载等）
                $status = 200;
                $remark = 'shop invalid.'.$task['shop_sign'];
            } else {
                $this->tagKey = 'flow:tag:update:order:'.$task['shop_sign'].':'.$task['request_data']['legacy_resource_id'];
                $isLock = $this->syncUpdateCache();
                if (!$isLock) {
                    continue;
                }
                list($status, $remark) = $this->platformOrderTag($shop, $task);
            }
            // handle result
            DB::transaction(function() use($task, $status, $remark) {
                $this->updateTaskMainStatus($task, $status, $remark);
            });
        }

        return true;
    }

    /**
     * @desc 平台用户标签
     * @param $shop
     * @param $task
     * @return array
     */
    private function platformOrderTag($shop, $task)
    {
        $tags = $this->getTagContents($task);
        if (empty($tags)) {
            return [200, 'Same tags, no need to synchronize'];
        }

        isRequestRest($task['shop_sign']);
        incrementRestRequest($task['shop_sign']);
        $shopifyRest = new \Plexins\MyShopify\ShopifyOrder($shop->shop_url, $shop->access_token);
        $platformResult = $shopifyRest->updateOrderTags($task['request_data']['legacy_resource_id'], $tags);

        return $this->handleSyncTagResult($platformResult);
    }

    /**
     * @desc 获取待同步标签内容
     * @param $task
     * @return array
     */
    private function getTagContents($task)
    {
        if (!isset($task['request_data']['setTags'])) { // 兼容旧数据
            return $task['request_data']['tags'];
        }

        $orderInfo = ShopifyOrder::where(['shop_sign' => $task['shop_sign'],
                    'legacy_resource_id' => $task['request_data']['legacy_resource_id'],
                    'customer_id' => $task['request_data']['customer_id']
                ])
            ->first(['tags', 'id']);

        $orderTags = is_array($orderInfo->tags) ? $orderInfo->tags : json_decode($orderInfo->tags,1);
        $diffTags = array_diff($task['request_data']['setTags'], $orderTags);
        $result = empty($diffTags) ? [] : array_merge($orderTags, $diffTags);

        if (!empty($result)) {
            $this->updateOrderTag($result, $orderInfo);
        }

        return $result;
    }

    /**
     * @desc 同步更新订单标签
     * @param $allTags
     * @param $orderInfo
     * @throws \Exception
     */
    private function updateOrderTag($allTags, $orderInfo)
    {
        ShopifyOrder::where(['id' => $orderInfo->id])
            ->where("tags", "=", \DB::raw("CAST('".json_encode($orderInfo->tags)."' as json)"))
            ->update(['tags' => json_encode($allTags)]);
    }


}
