<?php

namespace App\Services\Flow\Activity;

use App\Dto\Email\SingleCreateEmailDto;
use App\Dto\Sms\SmsReplaceDto;
use App\Enums\AutoTrigger\AutoTriggerEnum;
use App\Enums\Flow\FlowTypeEnum;
use App\Enums\Mail\MailSendMarkingTypeEnums;
use App\Enums\Mail\MailTemplateEnums;
use App\Enums\Merchant\MerchantShopAuthStatusEnum;
use App\Enums\Shopify\DiscountRuleMarketingTypeEnum;
use App\Enums\Sms\SmsReplaceEnum;
use App\Enums\Sms\SmsWeightTypeEnum;
use App\Enums\Subscriber\SystemSubscriptionStatusEnum;
use App\Helpers\Logger;
use App\Jobs\Flow\FlowSendEmailJob;
use App\Jobs\Flow\RechargePaySuccessJob;
use App\Lib\Code;
use App\Models\Flow\FlowShop;
use App\Models\Flow\FlowTask;
use App\Models\Mail\MailSend;
use App\Models\Mail\ShopMailTemplates;
use App\Models\MerchantCenter\MerchantShops;
use App\Models\Shopifys\ShopifyAbandonedCheckout;
use App\Services\Account\AccountService;
use App\Services\DotRecordService;
use App\Services\Flow\FlowTaskService;
use App\Services\Mail\BuildBatchCreateEmail\BuildBatchEmailFactory;
use App\Services\Mail\BuildBatchCreateEmail\CampaignBuildBatch;
use App\Services\Mail\MailService;
use App\Services\Mail\MailStoreService;
use App\Services\Shopify\DiscountService;
use App\Services\Sms\SmsReplace\SmsReplace;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class FlowSesService extends FlowSyncBase
{
    public $sendNumField = 'send_num';
    /**
     * @desc flow流程处理入口
     * @param $requestDataIni
     * @return array
     */
    public function handle($requestDataIni)
    {
        try {
            $errorInfo = '';
            $result = true;
            $requestData = $requestDataIni['business_data'];
            $requestData['currentNode'] = $requestDataIni['currentNode'];
            $requestData['flow_order_id'] = $requestDataIni['flow_order_id'];
            $this->checkMail($requestData['customer_info'] ?? '', $requestData['action'], $requestData['flow']);
            $this->syncTagTask($requestData);
        } catch (\Exception $e) {
            $result = false;
            if ($e->getCode() == Code::FLOW_BUSINESS_CHECK_ERROR) {
                $result = true;
            }
            $errorInfo = $e->getMessage().PHP_EOL.$e->getTraceAsString();
        }

        return [$result, $errorInfo];
    }

    /**
     * @desc 检查发件箱邮件有效性
     * @param array $customerInfo
     * @param int   $action       触发器类型
     * @throws \Exception
     */
    private function checkMail($customerInfo, $action, $flow)
    {
        if (empty($customerInfo) || empty($customerInfo['email']) || !isEffectiveEmail($customerInfo['email'])) {
            throw new \Exception('customer email is invalid.'.($customerInfo['customer_id'] ?? ''), Code::FLOW_BUSINESS_CHECK_ERROR);
        }

        // 弃单 已订阅（订阅短信或者邮件）限制订阅邮件状态
        if ($action == FlowTypeEnum::Checkout && isset($flow['rule'][0]['subscriber_type']) &&
            $flow['rule'][0]['subscriber_type'] == SystemSubscriptionStatusEnum::Subscribed
            && $customerInfo['email_subscription_status'] != SystemSubscriptionStatusEnum::Subscribed)
        {
             throw new \Exception('customer subscribed, email not subscribed'.$customerInfo['customer_id'], Code::FLOW_BUSINESS_CHECK_ERROR);
        }
        // 非弃单 严格限制邮件订阅状态
        if ($action != FlowTypeEnum::Checkout && (empty($customerInfo['email_subscription_status']) ||
                 $customerInfo['email_subscription_status'] != SystemSubscriptionStatusEnum::Subscribed)) {
            throw new \Exception('customer email not subscribed '.$customerInfo['customer_id'], Code::FLOW_BUSINESS_CHECK_ERROR);
        }
        if (isBlackEmail($customerInfo['email'])) {
            throw new \Exception(__('app.black_email')."::" . $customerInfo['email'], Code::FLOW_BUSINESS_CHECK_ERROR);
        }
    }

    /**
     * create sync task
     * 创建异步 邮件发送处理任务 - 创建待发送邮件 有两次调用shopify的动作 故异步处理
     * @params $requestData
     * @return boolean
     */
    private function syncTagTask($requestData)
    {
        $task = $this->formatData($requestData);
        // 定时任务 仅仅是为了 对异常情况进行补偿
        $result = FlowTask::create(array_merge($task, ['flow_sign' => $requestData['flow']['sign']]));
        // 异步直接处理
        if ($result) {
            $task['id'] = $result->id;
            FlowSendEmailJob::dispatch($task);
        }

        return $result ? true : false;
    }

    /**
     * @desc 定时任务异步处理待处理任务[补偿任务]
     * @param $shopSign
     * @param $pageSize
     * @param int $currentThread
     * @param int $totalThread
     * @return bool
     * @throws \Exception
     */
    public function doSyncToPlatform($shopSign, $pageSize, $currentThread = -1, $totalThread = 0)
    {
        $shopMap = [];
        $this->flowTaskService = new FlowTaskService(3);

        $this->taskWaitStatus = [FlowTask::DOING];
        $this->taskType = FlowTask::ORDER_EMAIL_TASK;
        $waitHandles = $this->getWaitDoTasks($shopSign, $pageSize, $currentThread, $totalThread);

        $mailSend = new MailSend();
        foreach($waitHandles as $key => $task) {
            $diffSeconds = time() - strtotime($task['created_at']);
            if ($diffSeconds < 3600) { // 60分钟内 无需处理 可能还在队列中等待执行
                continue;
            }
            $exist = $mailSend::where(['sign' => setFlowSendMailSign($task['id'])])->first(['id']);
            $isOverTime = $diffSeconds > 4*3600;  // 4小时内补偿有效，超时后再执行已没有意义，故终止执行
            if (!empty($exist) || $isOverTime) {  // 若已发送则 更新状态
                $request = [
                    'shop_sign' => $task['shop_sign'],
                    'flow_sign' => $task['request_data']['flow_sign'],
                    'customer_id' => $task['request_data']['customer_id'],
                    'flow_task_id' => $task['id'],
                ];
                $remark = $task['remark'].($isOverTime ? 'execute overtime' : '').' cronb do sync.';
                $this->handleDataSum($request, $isOverTime ? 0 : 200, $remark);
            } else { // 若未发送 则重新发送
                if (!isset($shopMap[$task['shop_sign']])) {
                    $shopMap[$task['shop_sign']] = getShopInfo($task['shop_sign']);
                }
                // 业务逻辑处理
                $task['exec_text'] = 'cronb';
                $this->handleBusinessData($shopMap[$task['shop_sign']], $task);
            }

        }

        return true;
    }

    /**
     * @desc 处理业务数据 创建待发送邮件+ 业务单据状态变更
     * @param $shop
     * @param $task
     * @return bool
     */
    public function handleBusinessData($shop, $task)
    {
        try {
            $handleStatus = 0;
            $request = $task['request_data'];
            $request['flow_task_sign'] = setFlowSendMailSign($task['id']);
            $request['flow_task_id'] = $task['id'];
            if (!$shop) { // 若店铺不可用（禁用、卸载等）
                throw new \Exception('shop invalid.'.$request['shop_sign'], Code::FLOW_BUSINESS_CHECK_ERROR);
            }
            // 创建待发送邮件
            $sendResult = $this->createWaitSendEmail($request); // 此方法有事务
            $handleStatus = $sendResult == true ? 200 : 0;
        } catch (\Exception $e) {
            $task['exec_text'] = ($task['exec_text']?? '').$e->getMessage();
            if ($e->getCode() == Code::FLOW_BUSINESS_CHECK_RETRY) { // 因必备业务数据不满足需重试
                $handleStatus = 400;
            }
        }
        // 处理发送结果
        $remark = $task['exec_text'] ?? '';
        $this->handleDataSum($request, $handleStatus, $remark);

        return true;
    }

    /**
     * @desc 创建待发送邮件
     * 在创建折扣规则和生成折扣的地方都有同步请求 shopify 故异步处理 邮件发送
     * @param $requestData
     * @return bool
     * @throws \Exception
     */
    private function createWaitSendEmail($requestData)
    {
        $account = new AccountService();
        $merchantShop = MerchantShops::where('shop_sign', $requestData['shop_sign'])
            ->where('auth_status', MerchantShopAuthStatusEnum::Success)
            ->first(['merchant_id']);
        // 生成折扣规则
        $discountRuleId = $this->createDiscountRule($requestData); // 有同步请求shopify
        $shop = $account->getSingleShopAccount($requestData['shop_sign']);
        $discountCode = app(DiscountService::class)->getDiscountCode(
            $shop,
            DiscountRuleMarketingTypeEnum::Flow,
            $discountRuleId,
            $requestData['discount_rules_data']
        );   //  有同步请求shopify
        // 需要折扣却无有效折扣可用
        if (!empty($requestData['discount_rules_data']) && empty($discountCode)) {
            throw new \Exception('discount code can not be empty.', Code::FLOW_BUSINESS_CHECK_RETRY);
        }
        // 关键字替换
        $smsReplaceDto = new SmsReplaceDto();
        $smsReplaceDto->discountConfig = $requestData['discount_rules_data'];
        $smsReplaceDto->discountCode   = $discountCode;
        $smsReplaceDto->shopSign       = $requestData['shop_sign'];
        $smsReplaceDto->email          = $requestData['email'];
        $smsReplaceDto->customerId     = $requestData['customer_id'];
        $smsReplaceDto->phone          = '';
        $smsReplaceDto->mailSendSign   = $requestData['flow_task_sign']; // 为了追踪当前流程是否已经成功生成了待发送邮件
        $smsReplaceDto->transferLabel  = SmsReplaceEnum::LINK_TRANSFER_LABEL;
        $smsReplaceDto->checkoutId     = '';
        // 获取邮件模板
        $templateInfo = $this->getShopEmailTemplate($merchantShop->merchant_id, $requestData['email_template_sign']);
        if (FlowTypeEnum::Checkout == $requestData['action'] && !empty($templateInfo->template_params)) { // 弃单展示订单详情
            $smsReplaceDto->transferLabel = SmsReplaceEnum::LINK_TRANSFER_TEXT;
            $smsReplaceDto->abandonedOrderDetailDiv = $templateInfo->template_params[0] ?? '';
            $smsReplaceDto->checkoutId = $requestData['legacy_resource_id'];
        }
        // 替换关键字
        $contentReplace = new SmsReplace();
        $contentHtml = filterEmailHtml($templateInfo->content_html);
        $content = $contentReplace->setDto($smsReplaceDto)->replace($contentHtml);
        $requestData['send_subject'] = $contentReplace->setDto($smsReplaceDto)->replace($requestData['send_subject']);
        //生成待发送邮件
        $name = 'flow-' . FlowTypeEnum::$topicMap[$requestData['action']];
        /** @var $build CampaignBuildBatch */
        $build = BuildBatchEmailFactory::getObject(MailSendMarkingTypeEnums::MARKING_TYPE_FLOW);
        $dto = $build->create($merchantShop->merchant_id, $name, $requestData['flow_sign']);
        $dto->operationId = 0;
        $dto->operationName = '';
        //todo 以前的数据存的没有活动编号，后期可以直接拿$requestData['flow_no']
        $dto->marketingNo = ''; // Flow-email
        if (!empty($requestData['flow_no'])){
            $dto->marketingNo = $requestData['flow_no'];
        }
        if (!empty($dto->marketingNo)){
            $marketingNo = FlowShop::query()->where('sign', $requestData['flow_sign'])->value('marketing_no');
            $dto->marketingNo = $marketingNo ?: 'Flow-email';
        }


        $dto->singleLists[] = $this->combineMailData($requestData, $shop, $content, $smsReplaceDto);
        app(MailStoreService::class)->setDto($dto)->store();
        // save dot data
        Redis::select(0);
        DotRecordService::addRecord('flow-create-email-'. (FlowTypeEnum::$topicMap[$requestData['action']] ?? ''), $requestData['flow_batch_sign'], $dto->toArray(), 'SUCCESS');
        return true;
    }

    /**
     * @param $requestData
     * @param $shop
     * @param $content
     * @param $smsReplaceDto
     * @return SingleCreateEmailDto
     * @throws \Exception
     */
    private function combineMailData($requestData, $shop, $content, $smsReplaceDto): SingleCreateEmailDto
    {
        $merchantId = getMerchantId($requestData['shop_sign']);
        $mailService = new MailService();
        $replyMail = !empty($requestData['reply_mail']) ? $requestData['reply_mail'] : $requestData['sender_mail'];
        $dto = new SingleCreateEmailDto();
        $dto->shop_sign = $requestData['shop_sign'];
        $dto->send_mail     = $mailService->replaceSenderEmail($requestData['sender_mail'], $merchantId);
        $dto->timezone      = $shop->timezone;
        $dto->send_name     = $requestData['sender_name'];
        $dto->sign          = $smsReplaceDto->mailSendSign;
        $dto->receive_name  = $requestData['first_name'] . ' ' . $requestData['last_name'];
        $dto->send_subject  = $requestData['send_subject'];
        $dto->supplier      = getMailSupplier($requestData['shop_sign'], $dto->send_mail);
        $dto->receive_mail  = $requestData['email'];
        $dto->e_send_time   = date('Y-m-d H:i:s');// 立即执行 无需转换时区 以服务器时间为准
        $dto->content_html  = $content;
        $dto->discount_code = $smsReplaceDto->discountCode;
        $dto->customer_id   = $requestData['customer_id'];
        $dto->reply_mail    = $mailService->replaceDefaultAddress($merchantId, $replyMail);
        $dto->mail_weight   = SmsWeightTypeEnum::WEIGHT_MARKETING;
        return $dto;
    }

    /**
     * 生成折扣码规则
     * @param $requestData
     * @return int
     */
    private function createDiscountRule($requestData): int
    {
        if (AutoTriggerEnum::DiscountRulesTrue != $requestData['is_discount_rules']) {
            return 0;
        }

        return app(DiscountService::class)->saveDiscountRule(
            app(AccountService::class)->getSingleShopAccount($requestData['shop_sign']),
            DiscountRuleMarketingTypeEnum::Flow,
            $requestData['flow_sign'],
            'flow-'.FlowTypeEnum::$topicMap[$requestData['action']],
            $requestData['discount_rules_data'] ?? []
        );
    }

    /**
     * @desc 获取店铺有效模板
     * @param $shopSign
     * @param $merchantId
     * @return string
     */
    private function getShopEmailTemplate($merchantId, $emailTemplateSign)
    {
        $where = ['status' => MailTemplateEnums::ENABLE,
            'merchant_id' => $merchantId,
            'sign' => $emailTemplateSign
        ];
        $column = ['content_html', 'template_params', 'template_discount'];
        $template = app(ShopMailTemplates::class)->where($where)->first($column);
        if (empty($template) || empty($template->content_html)) {
            throw new \Exception('email template invalid', Code::FLOW_BUSINESS_CHECK_ERROR);
        }

        return $template;
    }

    /**
     * @desc  处理次数及任务状态 抽出公共方法
     * @param $request
     * @param $handleStatus
     * @param $remark
     */
    private function handleDataSum($request, $handleStatus, $remark)
    {
        DB::transaction(function () use ($request, $handleStatus, $remark) {
            if ($handleStatus == 200) { //若处理成功则
                $this->sumSendNum($request['shop_sign'], $request['flow_sign'], $request['customer_id']);
            }
            $this->updateTaskMainStatus(['id' => $request['flow_task_id']], $handleStatus, $remark);
        });
    }

    private function formatData($requestData)
    {
        return [
            'flow_order_id' => $requestData['flow_order_id'],
            'shop_sign' => $requestData['shop_sign'],
            'run_at' => date('Y-m-d H:i:s'),
            'node' => $requestData['currentNode']->id,
            'status' => FlowTask::DOING,
            'task_type' => FlowTask::ORDER_EMAIL_TASK,
            'request_data' => [
                'shop_sign' => $requestData['shop_sign'],
                'action' => $requestData['action'],
                'flow_batch_sign' => $requestData['flow_batch_sign'],
                'flow_sign' => $requestData['flow']['sign'],
                'flow_no' => $requestData['flow']['marketing_no'],
                'email_subscription_status' => $requestData['customer_info']['email_subscription_status'],
                'customer_id' => $requestData['customer_info']['customer_id'],
                'first_name' => $requestData['customer_info']['first_name'],
                'last_name' => $requestData['customer_info']['last_name'],
                'email' => $requestData['customer_info']['email'],
                'sender_mail' => $requestData['currentNode']->data['sender_mail'] ?? '',
                'sender_name' => $requestData['currentNode']->data['sender_name'] ?? '',
                'send_subject' => $requestData['currentNode']->data['send_subject'] ?? '',
                'email_template_sign' => $requestData['currentNode']->data['email_template_sign'] ?? '',
                'is_discount_rules' => $requestData['currentNode']->data['is_discount_rules'] ?? 2,
                'discount_rules_data' => $requestData['currentNode']->data['discount_rules_data'] ?? [],
                'reply_mail' => $requestData['currentNode']->data['reply_mail'] ?? '',
                'legacy_resource_id' => $requestData['legacy_resource_id'] ?? '' // 来源平台单据ID
            ],
        ];
    }


}
