<?php


namespace App\Services\Flow\Activity;


use App\Enums\Flow\FlowTagSyncStatusEnum;
use App\Models\Flow\FlowShop;
use App\Models\Flow\FlowTask;
use Illuminate\Support\Facades\Redis;
use Plexins\MyShopify\MyShopifyClient;

class FlowTagBase extends FlowSyncBase
{
    protected $requestData = null;
    protected $tagKey = '';  // 标签追加防并发覆盖key
    protected $shopMap = []; // 缓存店铺信息
    /**
     * @desc 重写获取待处理标签方法
     * @param $shopSign
     * @param $pageSize
     * @param int $currentThread
     * @param int $totalThread
     * @return array
     */
    public function getWaitDoTasks($shopSign, $pageSize, $currentThread, $totalThread)
    {
        if ($this->flowSign) {
            $syncTagKey = getSyncUnsubscribeTagKey($shopSign[0], $this->flowSign);
            // 若操作手动执行将暂停的任务加入待同步
            if (Redis::connection('flow')->get($syncTagKey)) {
                $this->taskWaitStatus = [FlowTask::PAUSE];
            }
        }

        $waitHandles = parent::getWaitDoTasks($shopSign, $pageSize, $currentThread, $totalThread);

        if ($this->flowSign && !empty($waitHandles)) { // 在执行周期 若还没执行完成则延迟过期时间
            $expire = Redis::connection('flow')->ttl($syncTagKey);
            if ($expire < 30) {
                Redis::connection('flow')->setex($syncTagKey, 120, 1);
            }
        }

        return $waitHandles;
    }

    /**
     * @desc 创建异步推送任务
     * @param $taskType
     * @param $allTags
     * @return bool
     */
    protected function createSyncTagTask($taskType, $allTags)
    {
        $task = [
            'flow_order_id' => $this->requestData['flow_order_id'],
            'shop_sign' => $this->requestData['shop_sign'],
            'run_at' => date('Y-m-d H:i:s'),
            'node' => $this->requestData['currentNode']->id,
            'task_type' => $taskType,
            'request_data' => $allTags,
            'flow_sign' => $this->requestData['flow']['sign'] ?? '',
        ];

        // 若非订阅用户，则不同步到shopify 标定 暂停同步状态
        $syncUnsubTag = [];
        if ($this->requestData['customer_info']['sms_subscription_status'] != 1 &&
            $this->requestData['customer_info']['email_subscription_status'] != 1) {
            $task['status'] = FlowTask::PAUSE;
            $syncUnsubTag['sync_unsub_tag'] = FlowTagSyncStatusEnum::NEED_SYNC;
        }

        $result = FlowTask::create($task);

        // 添加待同步标识
        if ($result && !empty($syncUnsubTag)) {
            FlowShop::where('sign', $this->requestData['flow']['sign'])->update($syncUnsubTag);
        }

        return $result ? true : false;
    }

    /**
     * @desc 处理同步标签给平台后的返回结果
     * @param array $platformResult
     * @return bool
     */
    public function handleSyncTagResult($platformResult): array
    {
        if ($platformResult['code'] == MyShopifyClient::SUCCESS && !empty($platformResult['data'])) {
            $status = 200;
            $remark = '';
        } else {
            $status = FlowTask::FAIL;
            // 若是频率超出则 尝试重试
            if (isset($platformResult['data']['errors']) && stripos($platformResult['data']['errors'], 'API rate limit') !== false) {
                $status = 400;
            }
            $remark = json_encode($platformResult);
        }

        return [$status, $remark];
    }

    /**
     * @desc 针对并发覆盖更新 进行限制
     * @return void
     */
    public function syncUpdateCache()
    {
        $isLock = Redis::connection('flow')->setnx($this->tagKey, 1);

        if ($isLock) {
            Redis::connection('flow')->expire($this->tagKey, 15);
        }

        return $isLock;
    }

    /**
     * @desc 检查当前店铺是否可用
     * @param $shopSign
     * @return array
     */
    public function checkShop($shopSign)
    {
        if (!isset($this->shopMap[$shopSign])) {
           $this->shopMap[$shopSign] = getShopInfo($shopSign);
        }

        return $this->shopMap[$shopSign];
    }

}
