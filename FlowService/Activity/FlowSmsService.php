<?php


namespace App\Services\Flow\Activity;


use App\Enums\Flow\FlowTypeEnum;
use App\Enums\SmsContent\SmsContentSendStatusEnum;
use App\Enums\Subscriber\SystemSubscriptionStatusEnum;
use App\Enums\System\SmsServiceTypeEnum;
use App\Lib\Code;
use App\Models\AutoTrigger\SmsContent;
use App\Models\Flow\FlowOrdersLog;
use App\Services\DotRecordService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class FlowSmsService extends FlowSyncBase
{
    public $sendNumField = 'sms_send_num';
    /**
     * @desc flow 流程中处理发短信 入口
     * @param $requestData
     * @return array
     */
    public function handle($requestDataIni)
    {
        try {
            $errorInfo = '';
            $result = true;
            $requestData = $requestDataIni['business_data'];
            $requestData['currentNode'] = $requestDataIni['currentNode'];

            $this->checkSms($requestData);
            $this->sendSms($requestData);
            DB::transaction(function () use ($requestData) {
                $this->sumSendNum($requestData['shop_sign'], $requestData['flow']['sign'], $requestData['customer_id']);
            });
        } catch (\Exception $e) {
            if ($e->getCode() != Code::FLOW_BUSINESS_CHECK_ERROR) {
                $result = false;
            }
            $errorInfo = $e->getMessage().PHP_EOL.$e->getTraceAsString();
        }

        return [$result, $errorInfo];
    }

    /**
     * @desc 调用公共发送短信入口
     * @param array $requestDataIni
     * @throws \Exception
     */
    private function sendSms($requestData)
    {
        $pushData = [
            'action' => 'flow',
            'shop_sign' => $requestData['shop_sign'],
            'dot_record_type_id' => $requestData['flow_batch_sign'],
            'data' => [
                'customer_id' => $requestData['customer_id'] ?? 0,
                'phone' => $requestData['customer_info'] ? (string)$requestData['customer_info']['phone'] : '',
                'order_id' => in_array(($requestData['action'] ?? 0), FlowTypeEnum::OrderTypes) ? ($requestData['legacy_resource_id'] ?? 0) : 0,
                'checkout_id' => in_array(($requestData['action'] ?? 0), FlowTypeEnum::CheckoutTypes) ? ($requestData['legacy_resource_id'] ?? 0) : 0,
                'flow' => [
                    'id' => $requestData['flow']['id'] ?? 0,
                    'sign' => $requestData['flow']['sign'] ?? '',
                    'flow_no' => $requestData['flow']['marketing_no'] ?? '',
                    'name' => $requestData['flow']['name'] ?? '',
                    'discount_rules_data' => $requestData['currentNode']->data['discount_rules_data'] ?? [],
                    'is_discount_rules' => $requestData['currentNode']->data['is_discount_rules'] ?? 0,
                    'product_purchase_data' => $requestData['currentNode']->data['product_purchase_data'] ?? [],
                    'sms_template_sign' => (string)($requestData['currentNode']->data['sms_template_sign'] ?? ''),
                    'sms_content' => (string)($requestData['currentNode']->data['sms_content'] ?? ''),
                ],
                'merchant_id' => $requestData['flow']['merchant_id'],
                'operation_id' => $requestData['flow']['operation_id'],
                'operation_name' => $requestData['flow']['operation_name'],
            ]
        ];

        Redis::select(5);
        $pushResult = Redis::rpush('webhook:hand:down:yongzhong', json_encode($pushData));

        if ($pushResult && $requestData['action'] == FlowTypeEnum::Checkout &&
            $requestData['customer_info']['sms_subscription_status'] != SystemSubscriptionStatusEnum::Subscribed) {
            Redis::connection('flow')->setex($this->getUnsubSendCacheKey($requestData), 24*3600, 1);
        }

        // save dot data
        Redis::select(0);
        DotRecordService::addRecord('flow-sms-'. (FlowTypeEnum::$topicMap[$requestData['action']] ?? ''), $requestData['flow_batch_sign'], $pushData, 'SUCCESS');
    }

    /**
     * @desc  检查是否允许发送短信
     * @param array $requestData  请求参数
     * @throws \Exception
     */
    private function checkSms($requestData)
    {
        if (!isset($requestData['customer_info']) || empty($requestData['customer_info']['phone'])) {
            throw new \Exception('customer phone is empty '.$requestData['customer_id'], Code::FLOW_BUSINESS_CHECK_ERROR);
        }
        // 非弃单&非订阅 不发送短信
        if ($requestData['action'] != FlowTypeEnum::Checkout && (empty($requestData['customer_info']['sms_subscription_status']) ||
                $requestData['customer_info']['sms_subscription_status'] != SystemSubscriptionStatusEnum::Subscribed)) {
            throw new \Exception('customer sms not subscribed '.$requestData['customer_id'], Code::FLOW_BUSINESS_CHECK_ERROR);
        }
        // 弃单&若未订阅
        $this->checkSubscribeType($requestData);
    }

    /**
     * @desc 根据订阅用户类型 校验是否可以发送短信
     * @param $requestData
     * @throws \Exception
     */
    private function checkSubscribeType($requestData)
    {
        if ($requestData['action'] == FlowTypeEnum::Checkout &&
            $requestData['customer_info']['sms_subscription_status'] != SystemSubscriptionStatusEnum::Subscribed) {
            //  48小时之内有效 超过 不发 针对设置了定时器的节点
            if (strtotime($requestData['created_at']) + 2*86400 < time()) {
                throw new \Exception('customer not subscribed,sms overtime 48 hours.'.$requestData['customer_id'], Code::FLOW_BUSINESS_CHECK_ERROR);
            }
            // 48小时内 只能发送1条
            $sendRecord = $this->getSendRecord($requestData);
            if (!empty($sendRecord)) {
                Redis::connection('flow')->del($this->getUnsubSendCacheKey($requestData));
                throw new \Exception('customer had send sms, one limit. '.$requestData['customer_id'], Code::FLOW_BUSINESS_CHECK_ERROR);
            } elseif (Redis::connection('flow')->get($this->getUnsubSendCacheKey($requestData)) == 1) { // 因为发送SMS走异步队列有延迟故需要加一级缓存，防止在执行间隔重复执行
                throw new \Exception('customer had send sms, only one limit by all of flow. '.$requestData['customer_id'], Code::FLOW_BUSINESS_CHECK_ERROR);
            }
        }
    }

    /**
     * @desc 获取当前用户全部Flow 48小时内的发送记录
     *   注意只有非订阅用户弃单场景才有效，非订阅用户其他场景短信或者邮件都不会发送
     * @param $requestData
     * @return mixed
     */
    private function getSendRecord($requestData)
    {
        return SmsContent::where('marketing_type', SmsServiceTypeEnum::Flow)
            ->where('shop_sign', $requestData['shop_sign'])
            ->where('customer_id', $requestData['customer_id'])
            ->where('send_status', '!=', SmsContentSendStatusEnum::Fail)
            ->where('created_at', '>=', date('Y-m-d H:i:s', time()- 2*86400))
            ->first(['id']);
    }

    /**
     * @desc 非订阅用户- 防止同一个flow中多次发送短信故添加缓存
     * @param $requestData
     * @return string
     */
    private function getUnsubSendCacheKey($requestData)
    {
        return 'flow:unsublimit:'.$requestData['shop_sign'].':'.$requestData['customer_id'];
    }


}
