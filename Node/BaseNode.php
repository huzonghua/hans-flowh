<?php


namespace Plexins\Flow\Node;


class BaseNode
{
    const END_NODE = 'end';
    const START_NODE = 'Start_Event';
    const START_FILTER_NODE = 'Conditional_Start_Event';

    public $event = '';
    public $in = [];   // 父节点
    public $out = [];  // 子节点
    public $id = [];
    public $handle = null;

    public function setPropertyValue($property, $value) {
        if ($property == 'out' || $property == 'in' || $property == 'nout') {
           array_push($this->{$property}, $value);
        } else {
            $this->{$property} = $value;
        }
    }


}
