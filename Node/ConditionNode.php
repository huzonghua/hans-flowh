<?php


namespace Plexins\Flow\Node;


class ConditionNode extends BaseNode
{
    public $filter = null; // business filter setting
    public $nout = [];
    public $handle = 'filterParse';
    public $rule = ''; // trigger filter setting
}
