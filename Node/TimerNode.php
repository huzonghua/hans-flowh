<?php


namespace Plexins\Flow\Node;


class TimerNode extends BaseNode
{
    public $sleep = 0;
    public $handle = 'delayTask';
}
