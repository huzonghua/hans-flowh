<?php


namespace Plexins\Flow;


use Plexins\Flow\Tool\ParameterParseInterface;
use Plexins\Flow\Tool\ParseRule\FlowParseService;

class Flow
{
    public $nodes = null;

    public function __construct($flowRule = null, $nodeId = '')
    {
        $this->load($flowRule, $nodeId);
    }

    /**
     *
     * @param string $flow    json   node tree
     * @param string $nodeId  specified node
     */
    public function load($flowRule = null, $nodeId = '')
    {
        if ($flowRule == null) {
            return null;
        }
        $flowService = new FlowParseService();
        $this->nodes = $flowService->handle($flowRule, $nodeId);
        return $this;
    }

    public function getFlows()
    {
        return $this->nodes;
    }

    /**
     * get next node
     * Attention : support multiple nodes.
     * this function return array
     * @return array
     */
    public function nextNodes(): array
    {
        return $this->nodes->out;
    }

    /**
     * get previous nodes
     * Attention : support multiple nodes.
     * this function return array
     * @return array
     */
    public function prvNodes(): array
    {
        return $this->nodes->in;
    }


}
