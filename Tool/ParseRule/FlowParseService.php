<?php


namespace Plexins\Flow\Tool\ParseRule;


use Plexins\Flow\Exception\FlowParseException;
use Plexins\Flow\Exception\NodeException;
use Plexins\Flow\Node\ActivityNode;
use Plexins\Flow\Node\BaseNode;
use Plexins\Flow\Node\ConditionNode;
use Plexins\Flow\Node\NodeCode;
use Plexins\Flow\Node\TimerNode;
use Plexins\Flow\Tool\NodeParseInterface;

class FlowParseService implements NodeParseInterface
{
    public $nodeMap = [];
    public $bpMap = [];
    public $indexRule = [];
    private $flowNumLimit = 500;

    public function __construct()
    {
         $this->nodeMap = [
             'Start_Event' => BaseNode::class,
             'None_End_Event' => BaseNode::class,
             'Conditional_Start_Event' => ConditionNode::class,
             'Conditional_Intermediate_Event' => ConditionNode::class,
             'Timer_Intermediate_Event' => TimerNode::class,
             'Message_Intermediate_Event' => ActivityNode::class,
         ];
    }

    /**
     *         start
     *    2             3
     *  4    5       6    7
     *           8
     *     9        10
     *         end
     * desc : parse flow rule and transfer node to object.
     * Default start from root, however also support specified start node .
     * such as you can input "5" for second param.so flow tree will be like:
     *     5
     *        8
     *    9       10
     *       end
     * @param string $rule
     * @param string $nodeId
     * @return mixed|object | null
     */
    public function handle($rule, $nodeId = "")
    {
        $rule = is_array($rule) ? $rule : json_decode($rule,1);
        if (!is_array($rule) || !$rule) {
            throw new FlowParseException('flow rule invalid.');
        }
        $this->indexRule = array_column($rule, null, 'id'); //
        $eventNodes = array_column($rule, 'id', 'event');   // find start node
        unset($rule);

        if ($nodeId) {
            if (!isset($this->indexRule[$nodeId])) {
                throw new NodeException('invalid node ['.$nodeId.'] , please check.', NodeCode::NODE_INVALID);
            }
        } else {
            if ($eventNodes[BaseNode::START_FILTER_NODE]) {
                $nodeId = $eventNodes[BaseNode::START_FILTER_NODE];
            } elseif ($eventNodes[BaseNode::START_NODE]) {
                $nodeId = $eventNodes[BaseNode::START_NODE];
            } else {
                throw new \UnderflowException('invalid start node , please check');
            }
        }

        $startNode = $this->indexRule[$nodeId];

        return $this->transferObject($startNode);
    }

    /**
     * instance node object, parse rule and inject property into object.
     * @param $node
     * @return mixed|void
     * @throws \Exception
     */
    private function transferObject($node)
    {
        if ($node == '' || $node == null) {
            return ;
        }

        if ($this->flowNumLimit <= 0) {
            throw new \Exception('death circle occurred, please check node relation');
            return ;
        }
        $this->flowNumLimit--;

        $nodeObject = $this->getNodeClass($node['event']); // get node object
        $nodeObject->setPropertyValue('event', $node['event']);
        $nodeObject->setPropertyValue('id', $node['id']);

        if (property_exists($nodeObject, 'sleep')) {
            $nodeObject->setPropertyValue('sleep', $node['sleep']);
        }
        if (isset($node['handle'])) {
            $nodeObject->setPropertyValue('handle', $node['handle']);
        }
        if (isset($node['data'])) {
            $nodeObject->setPropertyValue('data', $node['data']);
        }
        if (isset($node['filter'])) {
            $nodeObject->setPropertyValue('filter', $node['filter']);
        }
        if (isset($node['rule'])) {
            $nodeObject->setPropertyValue('rule', $node['rule']);
        }

        $this->getOutNode($nodeObject, $node, 'out'); // children node
        $this->getOutNode($nodeObject, $node, 'nout'); // children node else

        // $this->getInNode($nodeObject, $node['in']);  // father node
        return $nodeObject;
    }

    /**
     * get current node's father nodes.
     * Because the node object is unique, so father node not need recursion
     * Only need instance node object in array 'in'
     * @param object $nodeObject    current node object
     * @param array  $fatherNodeIdArr current's father node id.
     * @return void
     */
    public function getInNode($nodeObject, $fatherNodeIdArr)
    {
        foreach($fatherNodeIdArr as $nodeId) {
            $tmpNode = $this->indexRule[$nodeId];
            $tmpObject = $this->getNodeClass($tmpNode['event'], $nodeId); // get node object
            $tmpObject->setPropertyValue('event', $tmpNode['event']);
            $tmpObject->setPropertyValue('id', $tmpNode['id']);

            $nodeObject->setPropertyValue('in', $tmpObject);
        }
    }

    /**
     * To find children nodes ,so it need recursion then all node will be like a tree (most case is  binary tree).
     * Default the start node is root. if specified start node, then the specified node is root
     * @param $nodeObject
     * @param array $node
     * @param string $type
     * @return void
     */
    private function getOutNode($nodeObject, $node, $type)
    {
        if (!isset($node[$type])) {
            return ;
        }
        foreach($node[$type] as $nodeId) {
            if ($nodeId == '' || $nodeId == null) {
                continue;
            }
            $tmpNode = $this->indexRule[$nodeId];

            $tmpObject = $this->transferObject($tmpNode);
            $nodeObject->setPropertyValue($type, $tmpObject);
        }
    }

    /**
     * cache instance object when created
     * @param $eventName
     * @param string $nodeId
     * @return mixed
     */
    private function getNodeClass($eventName, $nodeId = ''): mixed
    {
        try {
            if ($nodeId && isset($this->bpMap[$nodeId])) {
                return $this->bpMap[$nodeId];
            } else {
                $class = $this->nodeMap[$eventName];
            }
         } catch (NodeException $e) {
             throw new NodeException('node not defined', NodeCode::NODE_NO_DEFINED);
         }
        return new $class;
    }


}
