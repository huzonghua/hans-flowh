<?php


namespace Plexins\Flow\Tool;


interface NodeParseInterface
{
    public function handle($rule, $nodeId = "");
}
