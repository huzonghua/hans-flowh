<?php


namespace Plexins\Flow\Tool;

interface FilterParseInterface
{
    public function handle();
    public function iniRule($filterRule, $requestData);
}
