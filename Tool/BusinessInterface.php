<?php


namespace Plexins\Flow\Tool;

interface BusinessInterface
{
    public function handle($nodeInfo, $parentId = '');
    public function addLog($logInfo);
}
