<?php


namespace Plexins\Flow;


use Plexins\Flow\Node\BaseNode;
use Plexins\Flow\Tool\BusinessInterface;

class Engine
{
    public $flow = null;
    public $business = null;
    private $flowFlag = 0;
    public $currentNodeId = '';

    public function __construct(Flow $flow, BusinessInterface $busService = null)
    {
         $this->flow = $flow;
         $this->business = $busService;
    }

    /**
     * start execute current flow.
     * @param string $parentId
     * @return int
     */
    public function start($parentId = '') {
        $nodeObject = $this->flow->nodes;
        return $this->singleNode($nodeObject, $parentId);
    }

    /**
     * @desc return number.
     * 100 delay pause 延迟执行（定时任务）
     * 200 success     流程正常执行
     * 500 error       业务逻辑异常
     * 400 base rule fail  开始节点规则验证失败
     * @param $flowNode
     * @param string $parentId
     * @return int
     */
    public function singleNode($flowNode, $parentId = '')
    {
        $tmpNode = $flowNode;
        if (in_array($this->flowFlag, [100, 200, 500, 400], true)) {
            return $this->flowFlag;
        }

        $nextRoadTag = 'out';
        $result = $this->business->handle($tmpNode, $parentId);
        if ($tmpNode->handle == 'filterParse' && !$result) {      // 条件分支
            if ($tmpNode->event == BaseNode::START_FILTER_NODE) { // 条件过滤节点且为开始节点 过滤失败 直接终止流程
                $this->flowFlag = 400;
                $this->currentNodeId = $flowNode->id;
                return 400;
            }
            $nextRoadTag = 'nout';                                // 条件过滤节点 且 非开始节点且过滤失败，条件分支走 【否】流程
        }

        if ($tmpNode->handle != 'filterParse' && !$result) {    // 非条件过滤节点 结果为否 业务逻辑异常 终止
            $this->flowFlag = 500;
            $this->currentNodeId = $flowNode->id;
            return 500;
        }

        //  to next.
        $nextNodes = $tmpNode->{$nextRoadTag};                   // 获取下一个待执行节点
        if (empty($nextNodes)) {                                 // 若执行节点为空则 成功返回 100
            return 200;
        }

        if ($tmpNode->handle == 'delayTask') {                   // 定时任务 正常返回 100
            $this->flowFlag = 100;
            $this->currentNodeId = $flowNode->id;
            return 100;
        }

        // support multiple nodes
        foreach ($nextNodes as $key => $node) {                   // 遍历 同一层的全部节点（目前仅一个节点）
            $result = $this->singleNode($node, $tmpNode->id);     // 递归执行全部节点
            if ($result != 200) {
                break;
            }
        }
        return $result;
    }


}
